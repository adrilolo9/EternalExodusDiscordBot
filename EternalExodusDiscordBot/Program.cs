﻿using Topshelf;

namespace EternalExodusDiscordBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<BotMain>(s =>
                {
                    s.ConstructUsing(name => new BotMain());
                    s.WhenStarted(bm => bm.Start().GetAwaiter());
                    s.WhenStopped(bm => bm.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Eternal Exodus Discord Bot");
                x.SetDisplayName("EternalExodusBot");
                x.SetServiceName("EternalExodusBot");
            });
        }
    }
}
