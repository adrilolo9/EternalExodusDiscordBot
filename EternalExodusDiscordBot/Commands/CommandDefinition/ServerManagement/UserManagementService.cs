﻿using Discord;
using EternalExodusDiscordBot.Commands.Utils;
using ImageMagick;
using ImageMagick.Formats.Caption;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.ServerManagement
{
    public class UserManagementService
    {
        public string WelcomeUser(IUser user)
        {
            var welcomeImage = GetUserWelcomeImage(user);
            string welcomeImagePath = $"Resources/WelcomeImage/{user.Id}.png";

            using (FileStream fs = new FileStream(welcomeImagePath, FileMode.Create))
            {
                welcomeImage.WriteTo(fs);
                fs.Flush();
            }

            return welcomeImagePath;
        }

        private MemoryStream GetUserWelcomeImage(IUser user)
        {
            using (var images = new MagickImageCollection())
            {
                // Add the first image
                var background = new MagickImage(@"Resources\WelcomeImage\background.png");
                images.Add(background);

                var userImage = user.GetAvatarUrl(ImageFormat.Png, 256) ?? user.GetDefaultAvatarUrl();

                var itemImage = new MagickImage(userImage);
                MagickImage circle = new MagickImage(MagickColors.Transparent, 256, 256);
                itemImage.Resize(256, 256);
                circle.Draw(new DrawableFillColor(MagickColors.Red),
                            new DrawableCircle(128, 128, 128, 1));
                itemImage.Composite(circle, CompositeOperator.CopyAlpha);
                itemImage.Page = new MagickGeometry($"+384+26");
                images.Add(itemImage);

                var title = new MagickImage($"label:{user.Username}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Electrical.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(250,54,54),
                    Width = 1024,
                    Height = 50,
                    TextGravity = Gravity.Center,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 25
                    }
                });
                title.ColorSpace = ColorSpace.sRGB;
                title.Page = new MagickGeometry($"+0+345");
                images.Add(title);

                // Create a mosaic from both images
                using (var result = images.Mosaic())
                {
                    MemoryStream ms = new MemoryStream();
                    result.Write(ms);
                    return ms;
                }
            }
        }
    }
}
