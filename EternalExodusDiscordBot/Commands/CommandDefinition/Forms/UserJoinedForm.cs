﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.Events;
using EternalExodusDiscordBot.Commands.CommandDefinition.ServerManagement;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Forms
{
    public class UserJoinedForm
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private DiscordSocketClient context;

        public UserJoinedForm(DiscordSocketClient _context)
        {
            context = _context;
        }

        public async Task SendForm(IUser User)
        {
            try
            {
                await DeleteNewUserFromSpreadsheet(User.Id.ToString());
                var formEmbed = new EmbedUtils().UserJoinedFormEmbed("Formulario de acceso a Eternal Exodus", $"https://docs.google.com/forms/d/e/1FAIpQLSd_zQT06B3EhPVMTuQ9d5qJ1aRJqTe4gllUyiccML9_ZggPXA/viewform?usp=pp_url&entry.1608455022=i{User.Id}");
                var message = await User.SendMessageAsync(embed: formEmbed);
            }
            catch (Exception ex)
            {
                log.Error($"Error enviando formulario de usuario nuevo, exception: {ex.Message}");
            }
        }

        public async Task<List<EtxUserJoined>> CheckForNewUsers()
        {
            List<EtxUserJoined> listUserJoined = new List<EtxUserJoined>();

            var servicioForms = new GoogleFormsService().CrearServicio();
            String spreadsheetId = "1WzDPpuO_BoP3bJ43bp2S12UIDz7-pNjyOoHFNqNsI3Y";
            String range = "Respuestas de formulario 1!A2:L";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    servicioForms.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                foreach (var user in values)
                {
                    try
                    {
                        var discordUserField = user[1].ToString().Trim();
                        int htPosition = discordUserField.LastIndexOf('#');

                        if (htPosition != -1)
                        {
                            var username = discordUserField.Substring(0, htPosition);
                            string discriminatorString = discordUserField.Substring(htPosition + 1);
                            int discriminator;
                            bool hasDiscriminator = int.TryParse(discriminatorString, out discriminator);
                            if (hasDiscriminator == true && discriminator >= 0 && discriminator <= 9999)
                            {
                                var guildUsers = await (context.GetGuild(304330086612598785) as IGuild).GetUsersAsync();
                                var discordUser = guildUsers.FirstOrDefault(x => x.Username.ToUpper() == username.ToUpper() && x.Discriminator == discriminatorString);

                                if (discordUser != null && CheckUserAlreadyIn(discordUser) == false)
                                {
                                    listUserJoined.Add(new EtxUserJoined()
                                    {
                                        Mention = $"{discordUser.Username}#{discordUser.Discriminator}",
                                        DiscordId = discordUser.Id.ToString(),
                                        InvitadoPor = user[2].ToString(),
                                        ReferenciasServidor = user[3].ToString(),
                                        BautizadoPublicador = user[4].ToString(),
                                        Congregacion = user[5].ToString(),
                                        PropositoDios = user[6].ToString(),
                                        ReinoDios = user[7].ToString(),
                                        Jesus144k = user[8].ToString(),
                                        JehovaCualidad = user[9].ToString(),
                                        ObraUrgente = user[10].ToString(),
                                        TematicaAtalaya = user[11].ToString()
                                    });

                                    await DeleteNewUserFromSpreadsheet($"{discordUser.Username}#{discordUser.Discriminator}");
                                } else if (discordUser != null && CheckUserAlreadyIn(discordUser) == true)
                                {
                                    await DeleteNewUserFromSpreadsheet($"{discordUser.Username}#{discordUser.Discriminator}");
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error($"Error obteniendo los datos del nuevo usuario con discord id: {user[1].ToString()}, exception: {e.Message}");
                    }
                }
            }

            return listUserJoined;
        }

        public bool CheckUserAlreadyIn(IGuildUser user)
        {
            var userRoles = user.RoleIds;

            if (userRoles.Contains((ulong)759634845352722453) && userRoles.Contains((ulong)702848408951455755) && userRoles.Contains((ulong)662413046643425290) && userRoles.Contains((ulong)702854753629241375))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task DeleteNewUserFromSpreadsheet(string discordUser)
        {
            var servicioForms = new GoogleFormsService().CrearServicio();
            String spreadsheetId = "1WzDPpuO_BoP3bJ43bp2S12UIDz7-pNjyOoHFNqNsI3Y";
            String range = "Respuestas de formulario 1!A2:E";
            int sheetId = 570482237;
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                    servicioForms.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = getRequest.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                var user = values.FirstOrDefault(x => x[1].ToString() == discordUser);
                if (user != null)
                {
                    BatchUpdateSpreadsheetRequest content = new BatchUpdateSpreadsheetRequest();
                    Request delRequest = new Request()
                    {
                        DeleteDimension = new DeleteDimensionRequest()
                        {
                            Range = new DimensionRange()
                            {
                                SheetId = sheetId,
                                Dimension = "ROWS",
                                StartIndex = values.IndexOf(user) + 1,
                                EndIndex = values.IndexOf(user) + 2
                            }
                        }
                    };

                    List<Request> requests = new List<Request>();
                    requests.Add(delRequest);
                    content.Requests = requests;
                    servicioForms.Spreadsheets.BatchUpdate(content, spreadsheetId).Execute();
                }
            }

            await Task.CompletedTask;
        }

        public async Task AcceptUser(IMessage message, ISocketMessageChannel messageChannel)
        {
            var embed = message.Embeds.FirstOrDefault();

            if (embed != null)
            {
                var userId = Convert.ToUInt64(embed.Fields.FirstOrDefault(x => x.Name == "Discord ID").Value);
                var guild = (messageChannel as IGuildChannel).Guild;
                var roleJuegosETX = guild.GetRole(759634845352722453);
                var roleTituloJuegos = guild.GetRole(702848408951455755);
                var roleMiembrosETX = guild.GetRole(662413046643425290);
                var roleTituloMiembros = guild.GetRole(702854753629241375);
                var user = await guild.GetUserAsync(userId);
                var embedBuilder = embed.ToEmbedBuilder();

                var estadoField = embedBuilder.Fields.FirstOrDefault(x => x.Name == "Estado de la solicitud");
                estadoField.WithValue($"{EmoteUtils.GetEmoteStringByName("Accepted")} | Aceptada");

                embedBuilder.Color = new Discord.Color(0, 255, 0);

                await user.AddRolesAsync(new IRole[] { roleJuegosETX, roleTituloJuegos, roleMiembrosETX, roleTituloMiembros });

                await user.SendMessageAsync(embed: new EmbedUtils().SolicitudAprobadaEmbed());

                await (message as IUserMessage).ModifyAsync(x => x.Embed = embedBuilder.Build());

                var channel = context.GetChannel(304330086612598785) as ISocketMessageChannel;
                var welcomeImagePath = new UserManagementService().WelcomeUser(user);
                await channel.SendFileAsync(welcomeImagePath);
            }
        }

        public async Task DenyUser(IMessage message, ISocketMessageChannel messageChannel)
        {
            var embed = message.Embeds.FirstOrDefault();

            if (embed != null)
            {
                var userId = Convert.ToUInt64(embed.Fields.FirstOrDefault(x => x.Name == "Discord ID").Value);
                var user = context.GetUser(userId);
                var embedBuilder = embed.ToEmbedBuilder();

                var estadoField = embedBuilder.Fields.FirstOrDefault(x => x.Name == "Estado de la solicitud");
                estadoField.WithValue($"{EmoteUtils.GetEmoteStringByName("Denied")} | Denegada");

                embedBuilder.Color = new Discord.Color(255, 0, 0);

                await user.SendMessageAsync(embed: new EmbedUtils().SolicitudRechazadaEmbed());

                var guild = (message.Channel as IGuildChannel).Guild;
                await guild.AddBanAsync(user, 0, "Tu solicitud de entrada ha sido rechazada.");
                await guild.RemoveBanAsync(user);

                await (message as IUserMessage).ModifyAsync(x => x.Embed = embedBuilder.Build());
            }
        }
    }
}
