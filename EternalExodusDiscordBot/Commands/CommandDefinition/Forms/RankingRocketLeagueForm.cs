﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Forms
{
    public class RankingRocketLeagueForm
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private DiscordSocketClient context;

        public RankingRocketLeagueForm(DiscordSocketClient _context)
        {
            context = _context;
        }

        public async Task SendForm(IUser User)
        {
            try
            {
                var servicioForms = new GoogleFormsService().CrearServicio();

                await DeleteUserRanking(User);

                var formEmbed = new EmbedUtils().FormEmbed("Formulario inscripción rankings Rocket League", $"https://docs.google.com/forms/d/e/1FAIpQLSd_zQT06B3EhPVMTuQ9d5qJ1aRJqTe4gllUyiccML9_ZggPXA/viewform?usp=pp_url&entry.1608455022=i{User.Id}");

                var message = await User.SendMessageAsync(embed: formEmbed);

                var fechaInicio = DateTime.Now;
                var fechaFin = DateTime.Now.AddMinutes(10);

                var insertado = false;
                var error = false;

                while (DateTime.Now < fechaFin && insertado == false && error == false)
                {
                    await Task.Delay(5000);

                    String spreadsheetId = "1-yqd-KYu9lCABz5hG_9RCI1kYRlCKZo4P6jlD-P8sJw";
                    String range = "Respuestas de formulario 1!A2:E";
                    SpreadsheetsResource.ValuesResource.GetRequest request =
                            servicioForms.Spreadsheets.Values.Get(spreadsheetId, range);

                    ValueRange response = request.Execute();
                    IList<IList<Object>> values = response.Values;
                    if (values != null && values.Count > 0)
                    {
                        var user = values.FirstOrDefault(x => x[1].ToString().Replace("i", "") == User.Id.ToString());
                        if (user != null)
                        {
                            int platform;
                            int etxAcademy;

                            switch (user[3].ToString())
                            {
                                case "Steam":
                                    platform = 0;
                                    break;
                                case "PS4":
                                    platform = 1;
                                    break;
                                case "XBOX ONE":
                                    platform = 2;
                                    break;
                                default:
                                    platform = 0;
                                    break;
                            }

                            switch (user[4].ToString())
                            {
                                case "Eternal Exodus":
                                    etxAcademy = 1;
                                    break;
                                case "ETX Academy A":
                                    etxAcademy = 2;
                                    break;
                                case "ETX Academy B":
                                    etxAcademy = 3;
                                    break;
                                case "ETX Academy C":
                                    etxAcademy = 4;
                                    break;
                                default:
                                    etxAcademy = 0;
                                    break;
                            }

                            string userId = user[2].ToString();
                            long? steamId64 = null;

                            if (platform == 0)
                            {
                                var steam64 = await new SteamUtils().GetSteamId64(user[2].ToString());
                                if (steam64 == null)
                                {
                                    formEmbed = formEmbed.ToEmbedBuilder().WithColor(255, 0, 0).WithDescription("La ID de Steam proporcionada es incorrecta, reacciona de nuevo al emoji del canal #🥇╏ranking-etx para volver a empezar el proceso de registro.").WithThumbnailUrl("https://lolo.s-ul.eu/43lsavSJ.gif").Build();
                                    await message.ModifyAsync(x => x.Embed = formEmbed);
                                    error = true;
                                }
                                else
                                {
                                    steamId64 = steam64;
                                }
                            }
                            else if (platform == 1)
                            {
                                var tag = userId.Split('#')?.Last();
                                if (!string.IsNullOrEmpty(tag) && tag.Length == 4)
                                {
                                    var charArray = tag.ToCharArray();
                                    if (Char.IsDigit(charArray[0]) && Char.IsDigit(charArray[1]) && Char.IsDigit(charArray[2]) && Char.IsDigit(charArray[3]))
                                    {
                                        var idxof = userId.Substring(0, userId.LastIndexOf('#'));
                                    }
                                }
                            }

                            if (error == false)
                            {
                                using (var db = new EternalExodusEntities())
                                {
                                    var discordId = Convert.ToInt64(User.Id);
                                    db.RankingsRocketLeague.Add(new RankingsRocketLeague { DiscordId = discordId, UserId = userId, SteamId64 = steamId64, Username = userId, Platform = platform, ETXAcademy = etxAcademy, ETXStauts = 1010, ETXPosition = 0, v1 = 0, v2 = 0, v3 = 0, Rumble = 0, Dropshot = 0, Hoops = 0, Snowday = 0, v1Daily = 0, v2Daily = 0, v3Daily = 0, RumbleDaily = 0, DropshotDaily = 0, HoopsDaily = 0, SnowdayDaily = 0, Unranked = 0, UnrankedDaily = 0 });
                                    db.SaveChanges();
                                }

                                formEmbed = formEmbed.ToEmbedBuilder().WithColor(0, 255, 0).WithDescription("Registro completado correctamente!").WithThumbnailUrl("https://lolo.s-ul.eu/LTh15xEV.gif").Build();

                                await message.ModifyAsync(x => x.Embed = formEmbed);

                                insertado = true;
                            }
                        }
                    }
                }

                if (insertado == false && error == false)
                {
                    formEmbed = formEmbed.ToEmbedBuilder().WithColor(255, 0, 0).WithDescription("Se ha agotado el tiempo para rellenar el formulario, reacciona de nuevo al emoji del canal #🥇╏ranking-etx para volver a empezar el proceso de registro.").WithThumbnailUrl("https://lolo.s-ul.eu/43lsavSJ.gif").Build();
                    await message.ModifyAsync(x => x.Embed = formEmbed);
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error registrando a un usuario en los rankings de rl, exception: {ex.Message}");
            }
        }

        public async Task DeleteUserRanking(IUser User)
        {
            var servicioForms = new GoogleFormsService().CrearServicio();
            String spreadsheetId = "1-yqd-KYu9lCABz5hG_9RCI1kYRlCKZo4P6jlD-P8sJw";
            String range = "Respuestas de formulario 1!A2:E";
            int sheetId = 1065099738;
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                    servicioForms.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = getRequest.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                var user = values.FirstOrDefault(x => x[1].ToString().Replace("i", "") == User.Id.ToString());
                if (user != null)
                {
                    BatchUpdateSpreadsheetRequest content = new BatchUpdateSpreadsheetRequest();
                    Request delRequest = new Request()
                    {
                        DeleteDimension = new DeleteDimensionRequest()
                        {
                            Range = new DimensionRange()
                            {
                                SheetId = sheetId,
                                Dimension = "ROWS",
                                StartIndex = values.IndexOf(user) + 1,
                                EndIndex = values.IndexOf(user) + 2
                            }
                        }
                    };

                    List<Request> requests = new List<Request>();
                    requests.Add(delRequest);
                    content.Requests = requests;
                    servicioForms.Spreadsheets.BatchUpdate(content, spreadsheetId).Execute();
                }
            }

            using (var db = new EternalExodusEntities())
            {
                var userId = Convert.ToInt64(User.Id);
                var dbUser = db.RankingsRocketLeague.FirstOrDefault(x => x.DiscordId == userId);
                if (dbUser != null)
                {
                    db.RankingsRocketLeague.Remove(dbUser);
                    db.SaveChanges();
                }
            }

            await Task.CompletedTask;
        }
    }
}
