﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.CommandDefinition.Rankings;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using NLog;
using RiotSharp;
using RiotSharp.Misc;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Forms
{
    public class RankingLeagueOfLegendsForm
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private DiscordSocketClient context;

        public RankingLeagueOfLegendsForm(DiscordSocketClient _context)
        {
            context = _context;
        }

        public async Task SendForm(IUser User)
        {
            try
            {
                var servicioForms = new GoogleFormsService().CrearServicio();

                await DeleteUserRanking(User);

                var formEmbed = new EmbedUtils().FormEmbed("Formulario inscripción rankings League Of Legends", $"https://docs.google.com/forms/d/e/1FAIpQLSfk-pWgrxgUNLejZH9wAs6B9R1TPfiMzR1nsGS5aiCQAeQZMQ/viewform?usp=pp_url&entry.2005620554=i{User.Id}");

                var message = await User.SendMessageAsync(embed: formEmbed);

                var fechaInicio = DateTime.Now;
                var fechaFin = DateTime.Now.AddMinutes(10);

                var insertado = false;

                while (DateTime.Now < fechaFin && insertado == false)
                {
                    await Task.Delay(5000);

                    String spreadsheetId = "1rIjNATl3ip9AaDFApxn7xYwjmsJeGH_q0ALpSn5TOcY";
                    String range = "Respuestas de formulario 1!A2:D";
                    SpreadsheetsResource.ValuesResource.GetRequest request =
                            servicioForms.Spreadsheets.Values.Get(spreadsheetId, range);

                    ValueRange response = request.Execute();
                    IList<IList<Object>> values = response.Values;
                    if (values != null && values.Count > 0)
                    {
                        var user = values.FirstOrDefault(x => x[1].ToString().Replace("i", "") == User.Id.ToString());
                        if (user != null)
                        {
                            try
                            {
                                LeagueOfLegendsRankings lolRanks = new LeagueOfLegendsRankings();
                                var userId = Convert.ToInt64(User.Id);
                                var api = RiotApi.GetDevelopmentInstance(BotSettings.Default.LOLApiKey);
                                var summoner = await api.Summoner.GetSummonerByNameAsync((Region)Enum.Parse(typeof(Region), user[3].ToString()), user[2].ToString());
                                var leagues = await api.League.GetLeagueEntriesBySummonerAsync(summoner.Region, summoner.Id);

                                using (var db = new EternalExodusEntities())
                                {
                                    var player = new RankingsLeagueOfLegends
                                    {
                                        DiscordId = userId,
                                        SummonerId = summoner.Id,
                                        SummonerAccountId = summoner.AccountId,
                                        SummonerPuuid = summoner.Puuid,
                                        Region = summoner.Region.GetHashCode()
                                    };

                                    db.RankingsLeagueOfLegends.AddOrUpdate(player);

                                    db.SaveChanges();

                                    await lolRanks.UpdateSummonerStats(player);
                                }

                                formEmbed = formEmbed.ToEmbedBuilder().WithColor(0, 255, 0).WithDescription("Registro completado correctamente!").WithThumbnailUrl("https://lolo.s-ul.eu/LTh15xEV.gif").Build();

                                await message.ModifyAsync(x => x.Embed = formEmbed);

                                insertado = true;
                            }
                            catch (Exception ex)
                            {
                                log.Error($"Error registrando a un usuario en los rankings de lol, exception: {ex.Message}");

                                await DeleteUserRanking(User);

                                formEmbed = formEmbed.ToEmbedBuilder().WithColor(255, 0, 0).WithDescription("Error en el registro. Causas de error comunes: Región incorrecta, Summoner name incorrecto." + Environment.NewLine + "Haz de nuevo el proceso de registro (reacciona de nuevo al emoji del canal #🥇╏ranking-etx").WithThumbnailUrl("https://lolo.s-ul.eu/43lsavSJ.gif").Build();

                                await message.ModifyAsync(x => x.Embed = formEmbed);

                                insertado = true;
                            }
                        }
                    }
                }

                if (insertado == false)
                {
                    formEmbed = formEmbed.ToEmbedBuilder().WithColor(255, 0, 0).WithDescription("Se ha agotado el tiempo para rellenar el formulario, reacciona de nuevo al emoji del canal #🥇╏ranking-etx para volver a empezar el proceso de registro.").WithThumbnailUrl("https://lolo.s-ul.eu/43lsavSJ.gif").Build();
                    await message.ModifyAsync(x => x.Embed = formEmbed);
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error registrando a un usuario en los rankings de lol, exception: {ex.Message}");
            }
        }

        public async Task DeleteUserRanking(IUser User)
        {
            var servicioForms = new GoogleFormsService().CrearServicio();
            String spreadsheetId = "1rIjNATl3ip9AaDFApxn7xYwjmsJeGH_q0ALpSn5TOcY";
            String range = "Respuestas de formulario 1!A2:D";
            int sheetId = 121990529;
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                    servicioForms.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = getRequest.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                var user = values.FirstOrDefault(x => x[1].ToString().Replace("i", "") == User.Id.ToString());
                if (user != null)
                {
                    BatchUpdateSpreadsheetRequest content = new BatchUpdateSpreadsheetRequest();
                    Request delRequest = new Request()
                    {
                        DeleteDimension = new DeleteDimensionRequest()
                        {
                            Range = new DimensionRange()
                            {
                                SheetId = sheetId,
                                Dimension = "ROWS",
                                StartIndex = values.IndexOf(user) + 1,
                                EndIndex = values.IndexOf(user) + 2
                            }
                        }
                    };

                    List<Request> requests = new List<Request>();
                    requests.Add(delRequest);
                    content.Requests = requests;
                    servicioForms.Spreadsheets.BatchUpdate(content, spreadsheetId).Execute();
                }
            }

            using (var db = new EternalExodusEntities())
            {
                var userId = Convert.ToInt64(User.Id);
                var dbUser = db.RankingsLeagueOfLegends.FirstOrDefault(x => x.DiscordId == userId);
                if (dbUser != null)
                {
                    db.RankingsLeagueOfLegends.Remove(dbUser);
                    db.SaveChanges();
                }
            }

            await Task.CompletedTask;
        }
    }
}
