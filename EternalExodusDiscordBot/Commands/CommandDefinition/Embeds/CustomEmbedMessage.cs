﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Embeds
{
    public class CustomEmbedMessage
    {
        public async Task NewCustomEmbed(IUserMessage eventMessage, string time, ICommandContext context)
        {
            var splitTime = time.Split(',');
            var duration = splitTime.Length > 2 ? TimeSpan.FromMinutes(Convert.ToDouble(splitTime[2])).TotalMilliseconds : TimeSpan.FromMinutes(30).TotalMilliseconds;
            var startTime = DateTime.Today.AddDays(Convert.ToDouble(splitTime[0])).AddMilliseconds(TimeSpan.Parse(splitTime[1]).TotalMilliseconds);
            var endTime = startTime.AddMilliseconds(duration);
            CountdownEmbed cemb;

            using (var db = new EternalExodusEntities())
            {
                cemb = new CountdownEmbed { EmbedId = Convert.ToInt64(eventMessage.Id), ChannelId = Convert.ToInt64(context.Channel.Id), GuildId = Convert.ToInt64(context.Guild.Id), StartTime = startTime, EndTime = endTime, Ended = false, CreationTime = DateTime.Now };
                db.CountdownEmbed.Add(cemb);
                db.SaveChanges();
            }

            EmbedBuilder embed = new EmbedBuilder()
            {
                Description = $"&eventodes {eventMessage.Id} Descripcion del embed" + Environment.NewLine + $"&eventofoto {eventMessage.Id} Url imagen" + Environment.NewLine + $"&eventominiatura {eventMessage.Id} Url imagen (miniatura)" + Environment.NewLine + $"&eventourl {eventMessage.Id} Url evento (twitch, youtube...)" + Environment.NewLine + $"&borrarevento {eventMessage.Id}" + Environment.NewLine + Environment.NewLine + "🔔 **Toca la campana para ser notificado cuando el evento comience.**"
            }
                .WithColor(new Discord.Color(145, 71, 255))
                .WithAuthor($"&editembtime {eventMessage.Id} d,h,m")
                .WithTitle($"&eventotitulo {eventMessage.Id} Titulo del embed")
                .WithUrl("https://www.twitch.tv/eternalexodusclan")
                .WithFooter($"Message ID: {eventMessage.Id} | Embed type: 2")
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://lolo.s-ul.eu/1gXG6E6Y");

            await eventMessage.ModifyAsync(x => x.Embed = embed.Build());
            await eventMessage.AddReactionAsync(new Emoji("🔔"));
            await context.Message.DeleteAsync();
            CountdownEmbed(cemb, context.Client as DiscordSocketClient).GetAwaiter();
        }

        public async Task CountdownEmbed(CountdownEmbed cemb, DiscordSocketClient client)
        {
            bool isTimeEnded = false;
            CancellationTokenSource cts = new CancellationTokenSource();
            EmbedUtils.cancellationTokens.Add((ulong)cemb.EmbedId, cts);
            SocketTextChannel channel = client.GetChannel((ulong)cemb.ChannelId) as SocketTextChannel;
            IReadOnlyCollection<SocketGuildUser> guildUsers = client.GetGuild((ulong)cemb.GuildId).Users;
            IUserMessage message = channel.GetMessageAsync((ulong)cemb.EmbedId).Result as IUserMessage;
            EmbedBuilder emb = message.Embeds.First().ToEmbedBuilder();


            while (isTimeEnded == false && DateTime.Now <= cemb.StartTime)
            {
                message = channel.GetMessageAsync((ulong)cemb.EmbedId).Result as IUserMessage;
                emb = message.Embeds.First().ToEmbedBuilder();
                if ((cemb.StartTime - DateTime.Now).TotalMilliseconds <= 60000)
                {
                    isTimeEnded = true;
                    emb.WithAuthor($"{cemb.StartTime.ToString("dd/MM/yyyy HH:mm")} - Falta menos de un minuto  |  Duración: {(cemb.EndTime - cemb.StartTime).TotalMinutes}m");
                    await message.ModifyAsync(x => x.Embed = emb.Build());
                    await Task.Delay(Convert.ToInt32(Math.Ceiling((cemb.StartTime - DateTime.Now).TotalMilliseconds)), cts.Token);
                }
                else
                {
                    var remainingTime = cemb.StartTime - DateTime.Now;
                    emb.WithAuthor($"{cemb.StartTime.ToString("dd/MM/yyyy HH:mm")} - Falta {remainingTime.Days}d {remainingTime.Hours}h {remainingTime.Minutes}m  |  Duración: {(cemb.EndTime - cemb.StartTime).TotalMinutes}m");
                    await message.ModifyAsync(x => x.Embed = emb.Build());
                    await Task.Delay(60000, cts.Token);
                }
            }

            if (cemb.EndTime > DateTime.Now)
            {
                emb.WithAuthor($"{cemb.StartTime.ToString("dd/MM/yyyy HH:mm")} - En directo!  |  Duración: {(cemb.EndTime - cemb.StartTime).TotalMinutes}m");
                emb.WithColor(0, 255, 0);
                await message.ModifyAsync(x => x.Embed = emb.Build());

                List<CountdownEmbedNotifications> notificationsList;

                using (var db = new EternalExodusEntities())
                {
                    notificationsList = db.CountdownEmbedNotifications.Where(x => x.EmbedId == cemb.EmbedId).ToList();
                }

                foreach (var userId in notificationsList)
                {
                    var user = guildUsers.FirstOrDefault(x => x.Id == (ulong)userId.UserId);
                    var dmChannel = user.GetOrCreateDMChannelAsync().Result;
                    await dmChannel.SendMessageAsync(embed: emb.Build());
                }

                await Task.Delay(Convert.ToInt32(Math.Ceiling((cemb.EndTime - cemb.StartTime).TotalMilliseconds - (DateTime.Now - cemb.StartTime).TotalMilliseconds)), cts.Token);
            }

            emb.WithAuthor($"Evento terminado.");
            emb.WithColor(255, 0, 0);
            await message.ModifyAsync(x => x.Embed = emb.Build());

            var cancelKey = EmbedUtils.cancellationTokens.FirstOrDefault(x => x.Key == (ulong)cemb.EmbedId).Key;
            EmbedUtils.cancellationTokens.Remove(cancelKey);

            using (var db = new EternalExodusEntities())
            {
                cemb.Ended = true;
                db.CountdownEmbed.AddOrUpdate(cemb);
                db.SaveChanges();
            }
        }
    }
}
