﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.Backups;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Backups
{
    public class BackupService
    {
        public async Task SetChannelBackup(ICommandContext context)
        {
            if (context.User.IsBot == false)
            {
                using (var db = new EternalExodusEntities())
                {
                    await context.Message.DeleteAsync();
                    var embedUtils = new EmbedUtils();
                    var channelId = Convert.ToInt64(context.Channel.Id);
                    var backupExist = db.BackupChannel.FirstOrDefault(x => x.ChannelId == channelId || x.ChannelName == context.Channel.Name);
                    if (backupExist == null)
                    {
                        db.BackupChannel.Add(new BackupChannel() { ChannelId = channelId, ChannelName = context.Channel.Name });
                        await context.User.SendMessageAsync(embed: embedUtils.SuccesEmbed($"Se ha añadido correctamente el canal {context.Channel.Name} a la lista de canales de backup."));
                        BackupChannelMessages(context.Client as DiscordSocketClient, context.Channel.Id).GetAwaiter();
                    }
                    else
                    {
                        db.BackupChannel.Remove(backupExist);
                        await context.User.SendMessageAsync(embed: embedUtils.SuccesEmbed($"Se ha eliminado correctamente el canal {context.Channel.Name} de la lista de canales de backup."));
                    }
                    db.SaveChanges();
                }
            }
            await Task.CompletedTask;
        }

        public async Task BackupChannelMessages(DiscordSocketClient client, ulong channel)
        {
            EmbedUtils embedUtils = new EmbedUtils();
            var channelDisc = client.GetChannel(channel) as ISocketMessageChannel;
            var channelId = Convert.ToInt64(channel);
            var messageContainers = channelDisc.GetMessagesAsync(10000).Select(x => x.ToList());
            int messageOrder = 0;
            using (var db = new EternalExodusEntities())
            {
                var oldBackup = db.BackupChannelMessage.Where(x => x.ChannelId == channelId || x.ChannelName == channelDisc.Name).OrderByDescending(x => x.MessageOrder).ToList();
                db.BackupChannelMessage.RemoveRange(oldBackup);
                await foreach (var messageLists in messageContainers)
                {
                    foreach (var message in messageLists)
                    {
                        if (message.Content != "&backup" || message.Content != "%backup")
                        {
                            var messageBackup = new MessageBackup(message);
                            var serializedMessage = JsonConvert.SerializeObject(messageBackup);

                            db.BackupChannelMessage.Add(new BackupChannelMessage()
                            {
                                MessageBackupId = Guid.NewGuid().ToString().Replace("-", ""),
                                ChannelId = channelId,
                                ChannelName = channelDisc.Name,
                                Message = serializedMessage,
                                MessageOrder = messageOrder++
                            });
                        }
                    }
                }

                db.SaveChanges();
            }

            var logChannel = client.GetChannel(742102319113437237) as ISocketMessageChannel;
            await logChannel.SendMessageAsync(embed: embedUtils.SuccesEmbed($"Se ha realizado un backup de los mensajes del canal {channelDisc.Name} correctamente."));

            await Task.CompletedTask;
        }

        public async Task RestoreChannelMessages(ICommandContext context, string channelName)
        {
            WebClient client;
            EmbedUtils embedUtils = new EmbedUtils();

            await context.Message.DeleteAsync();
            var channelList = context.Guild.GetChannelsAsync().Result.ToList(); 
            var channel = string.IsNullOrEmpty(channelName) ? context.Channel : channelList.FirstOrDefault(x => x.Name == channelName) as IMessageChannel;

            if (channel != null)
            {
                using (var db = new EternalExodusEntities())
                {

                    var channelId = Convert.ToInt64(channel.Id);
                    var messages = db.BackupChannelMessage.Where(x => x.ChannelId == channelId || x.ChannelName == channel.Name).OrderByDescending(x => x.MessageOrder).ToList();
                    foreach (var message in messages)
                    {
                        var bMessage = JsonConvert.DeserializeObject<MessageBackup>(message.Message);
                        var dMessage = BackupToDiscordMessage(bMessage);
                        if (string.IsNullOrEmpty(dMessage.Attachment))
                        {
                            await context.Channel.SendMessageAsync(dMessage.MessageContent, false, dMessage.Embed);
                        }
                        else
                        {
                            client = new WebClient();
                            var fileName = dMessage.Attachment.Split('/').Last();
                            client.DownloadFile(new Uri(dMessage.Attachment), $@"{fileName}");
                            await context.Channel.SendFileAsync(fileName, dMessage.MessageContent, false, dMessage.Embed);
                            File.Delete(fileName);
                        }
                    }
                }
            }
            else
            {
                await context.Channel.SendMessageAsync(embed: embedUtils.ErrorEmbed($"No se ha encontrado el canal {channelName}"));
            }
            
            await Task.CompletedTask;
        }

        private DiscordMessage BackupToDiscordMessage(MessageBackup message)
        {
            var dmessage = new DiscordMessage();
            dmessage.MessageContent = message.MessageContent;
            dmessage.Attachment = message.Attachment != null ? message.Attachment.Url : null;

            if (message.Embed != null)
            {
                var dembedBuilder = new EmbedBuilder();

                dembedBuilder.WithDescription(message.Embed.Description);
                dembedBuilder.WithTitle(message.Embed.Title);
                dembedBuilder.WithUrl(message.Embed.Url);
                if (message.Embed.Timestamp.HasValue)
                {
                    dembedBuilder.WithTimestamp(message.Embed.Timestamp.Value);
                }
                
                if (message.Embed.Author != null)
                {
                    var authorBuilder = new EmbedAuthorBuilder()
                    {
                        IconUrl = message.Embed.Author.IconUrl,
                        Name = message.Embed.Author.Name,
                        Url = message.Embed.Url
                    };
                    dembedBuilder.WithAuthor(authorBuilder);
                }
                if (message.Embed.Color != null)
                {
                    var color = new Discord.Color(message.Embed.Color.R, message.Embed.Color.G, message.Embed.Color.B);
                    dembedBuilder.WithColor(color);
                }
                if (message.Embed.Footer != null)
                {
                    var footer = new EmbedFooterBuilder()
                    {
                        IconUrl = message.Embed.Footer.IconUrl,
                        Text = message.Embed.Footer.Text
                    };
                    dembedBuilder.WithFooter(footer);
                }
                if (message.Embed.Image != null)
                {
                    dembedBuilder.WithImageUrl(message.Embed.Image.Url);
                }
                if (message.Embed.Thumbnail != null)
                {
                    dembedBuilder.WithThumbnailUrl(message.Embed.Thumbnail.Url);
                }
                if (message.Embed.Fields != null && message.Embed.Fields.Count > 0)
                {
                    List<EmbedFieldBuilder> embedFields = new List<EmbedFieldBuilder>();
                    foreach (var field in message.Embed.Fields)
                    {
                        embedFields.Add(new EmbedFieldBuilder().WithName(field.Name).WithValue(field.Value).WithIsInline(field.Inline));
                    }
                    dembedBuilder.WithFields(embedFields);
                }

                dmessage.Embed = dembedBuilder.Build();
            }

            return dmessage;
        }
    }
}
