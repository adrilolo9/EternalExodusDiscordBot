﻿using Discord;
using EternalExodusDiscordBot.Commands.Utils;
using HtmlAgilityPack;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.JW
{
    public class DailyText
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private WebUtils webUtils = new WebUtils();

        public Embed GetDailyText()
        {
            string titulo = "";
            string content = "";

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(webUtils.GetPageHtml($"https://wol.jw.org/es/wol/dt/r4/lp-s/{DateTime.Today.Year}/{DateTime.Today.Month}/{DateTime.Today.Day}"));
            var nodes = htmlDocument.DocumentNode.SelectNodes("//div[@class='itemData scalableui']").First().InnerHtml;
            htmlDocument.LoadHtml(nodes);
            var dia = htmlDocument.DocumentNode.SelectNodes("//header").First().FirstChild.NextSibling.InnerText;
            var tituloNode = htmlDocument.DocumentNode.SelectNodes("//p[@class='themeScrp']").First().InnerHtml.Replace("<em>", "").Replace("</em>", "");
            var contentNode = htmlDocument.DocumentNode.SelectNodes("//div[@class='pGroup']").First().ChildNodes.First(x => x.Name.Equals("p")).InnerHtml.Replace("<em>", "").Replace("</em>", "");

            htmlDocument.LoadHtml(tituloNode);
            foreach (var node in htmlDocument.DocumentNode.ChildNodes)
            {
                if (node.Name.Equals("a"))
                {
                    titulo += $"[{node.InnerText}](https://wol.jw.org{node.Attributes.First(x => x.Name.Equals("href")).Value})";
                }
                else
                {
                    titulo += node.InnerText;
                }
            }

            htmlDocument.LoadHtml(contentNode);
            foreach (var node in htmlDocument.DocumentNode.ChildNodes)
            {
                if (node.Name.Equals("a"))
                {
                    content += $"[{node.InnerText}](https://wol.jw.org{node.Attributes.First(x => x.Name.Equals("href")).Value})";
                }
                else
                {
                    content += node.InnerText;
                }
            }


            EmbedFieldBuilder tituloEmbed = new EmbedFieldBuilder()
                .WithName("Texto")
                .WithValue(titulo);

            Embed textoEmbed = new EmbedBuilder()
            {
                Description = content.Length > 2048 ? content.Substring(0, 2047) : content
            }
                .WithColor(new Discord.Color(150, 150, 150))
                .WithAuthor("📖 " + dia)
                .AddField(tituloEmbed)
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://assetsnffrgf-a.akamaihd.net/themes/content-theme/images/siteLogo-jworg-print.png")
                .Build();

            return textoEmbed;
        }
    }
}
