﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague
{
    public class RocketStats
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private WebUtils webUtils = new WebUtils();

        public RankingsRocketLeague GetPlayerStats(RankingsRocketLeague userRanking)
        {
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();

                htmlDocument.LoadHtml(webUtils.GetPageHtml($"https://rocketleague.tracker.network/rocket-league/profile/{Enum.GetName(typeof(PlatformEnum), userRanking.Platform)}/{(userRanking.Platform == 0 ? userRanking.SteamId64.Value.ToString() : userRanking.UserId)}/overview"));

                var scriptNode = htmlDocument.DocumentNode.SelectNodes("//script").FirstOrDefault(x => x.InnerText.StartsWith("window.__INITIAL_STATE__"));
                var jsonStats = scriptNode.InnerText.Split(new string[] { "(function()" }, StringSplitOptions.RemoveEmptyEntries)[0].Replace("window.__INITIAL_STATE__=", "").TrimEnd(';');

                JToken stats = JObject.Parse(jsonStats)["stats-v2"]["standardProfiles"][$"rocket-league|{Enum.GetName(typeof(PlatformEnum), userRanking.Platform)}|{(userRanking.Platform == 0 ? userRanking.SteamId64.Value.ToString() : userRanking.UserId.ToLowerInvariant())}"];

                var username = stats["platformInfo"]["platformUserHandle"].ToString();
                if (!string.IsNullOrEmpty(username))
                {
                    userRanking.Username = username;
                }

                var rankings = stats["segments"].Children().Where(x => (string)x["type"] == "playlist").ToList();
                var ranksList = new Dictionary<string, int>();
                foreach (var rank in rankings)
                {
                    ranksList.Add(rank["metadata"]["name"].ToString(), Convert.ToInt32(rank["stats"]["rating"]["value"]));
                }

                using (var db = new EternalExodusEntities())
                {
                    userRanking.Unranked = ranksList.ContainsKey("Un-Ranked") ? ranksList.FirstOrDefault(x => x.Key == "Un-Ranked").Value : 0;
                    userRanking.v1 = ranksList.ContainsKey("Ranked Duel 1v1") ? ranksList.FirstOrDefault(x => x.Key == "Ranked Duel 1v1").Value : 0;
                    userRanking.v2 = ranksList.ContainsKey("Ranked Doubles 2v2") ? ranksList.FirstOrDefault(x => x.Key == "Ranked Doubles 2v2").Value : 0;
                    userRanking.v3 = ranksList.ContainsKey("Ranked Standard 3v3") ? ranksList.FirstOrDefault(x => x.Key == "Ranked Standard 3v3").Value : 0;
                    userRanking.Hoops = ranksList.ContainsKey("Hoops") ? ranksList.FirstOrDefault(x => x.Key == "Hoops").Value : 0;
                    userRanking.Rumble = ranksList.ContainsKey("Rumble") ? ranksList.FirstOrDefault(x => x.Key == "Rumble").Value : 0;
                    userRanking.Dropshot = ranksList.ContainsKey("Dropshot") ? ranksList.FirstOrDefault(x => x.Key == "Dropshot").Value : 0;
                    userRanking.Snowday = ranksList.ContainsKey("Snowday") ? ranksList.FirstOrDefault(x => x.Key == "Snowday").Value : 0;

                    db.RankingsRocketLeague.AddOrUpdate(userRanking);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                log.Error($"Error obteniendo estadisticas para el userid {userRanking.UserId}, error: {e.Message}");
            }

            return userRanking;
        }

        public void UpdateDailyRanks()
        {
            using (var db = new EternalExodusEntities())
            {
                var players = db.RankingsRocketLeague.ToList();
                foreach (var player in players)
                {
                    player.UnrankedDaily = player.Unranked;
                    player.v1Daily = player.v1;
                    player.v2Daily = player.v2;
                    player.v3Daily = player.v3;
                    player.RumbleDaily = player.Rumble;
                    player.DropshotDaily = player.Dropshot;
                    player.HoopsDaily = player.Hoops;
                    player.SnowdayDaily = player.Snowday;
                    db.RankingsRocketLeague.AddOrUpdate(player);
                }
                db.SaveChanges();
            }
        }

        public void UpdateETXAcademyPositions()
        {
            using (var db = new EternalExodusEntities())
            {
                var etxPlayers = db.RankingsRocketLeague.Where(x => x.ETXAcademy != 0).ToList();
                var countAcademies = etxPlayers.Where(x => x.ETXAcademy != 0).Select(x => x.ETXAcademy).Distinct().Count();

                for (int i = 1; i <= countAcademies; i++)
                {
                    var currentAcademy = etxPlayers.Where(x => x.ETXAcademy == i).OrderByDescending(x => x.v3).ToList();
                    foreach (var player in currentAcademy)
                    {
                        player.ETXPosition = currentAcademy.IndexOf(player) + 1;
                        db.RankingsRocketLeague.AddOrUpdate(player);
                    }
                }

                db.SaveChanges();
            }
        }
    }
}
