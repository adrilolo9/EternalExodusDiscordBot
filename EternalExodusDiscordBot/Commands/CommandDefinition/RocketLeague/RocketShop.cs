﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.MagickNet;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.Utils;
using HtmlAgilityPack;
using ImageMagick;
using ImageMagick.Formats.Caption;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague
{
    public class RocketShop
    {
        private WebUtils webUtils = new WebUtils();

        #region Deprecated Methods

        //public async Task<string> GetItemShopEmbed()
        //{
        //    var shopImage = GenerateItemShopImage();
        //    var imageUrl = await webUtils.UploadImageToSul(shopImage.image);

        //    EmbedFieldBuilder priceField = new EmbedFieldBuilder()
        //        .WithName($"Rango de precios")
        //        .WithValue($"{shopImage.minPrice} - {shopImage.maxPrice} {EmoteUtils.GetEmoteStringByName("Creditos")}");

        //    EmbedFieldBuilder featuredExpiryField = new EmbedFieldBuilder()
        //        .WithName($"Objetos destacados disponibles hasta")
        //        .WithValue(DateTime.Today.AddDays(shopImage.featuredExpiry).AddHours(22));

        //    EmbedFieldBuilder dailyExpiryField = new EmbedFieldBuilder()
        //        .WithName($"Objetos diarios disponibles hasta")
        //        .WithValue(DateTime.Today.AddDays(1).AddHours(22));

        //    Embed embed = new EmbedBuilder()
        //        .WithColor(new Discord.Color(237, 19, 128))
        //        .WithTitle($"**{new Emoji(":shopping_cart:")} | TIENDA DIARIA - ROCKET LEAGUE**")
        //        .WithFields(new EmbedFieldBuilder[] { priceField, featuredExpiryField, dailyExpiryField })
        //        .WithImageUrl(imageUrl)
        //        .Build();

        //    return embed;
        //}

        #endregion

        public string GetItemShop()
        {
            var shopImage = GenerateItemShopImage();
            string shopImagePath = "Resources/RocketShop/shop.png";

            using (FileStream fs = new FileStream(shopImagePath, FileMode.Create))
            {
                shopImage.image.WriteTo(fs);
                fs.Flush();
            }

            return shopImagePath;
        }
        private (MemoryStream image, int minPrice, int maxPrice, int featuredExpiry) GenerateItemShopImage()
        {
            var items = GetItems();
            var minPrice = items.OrderBy(x => x.Price).FirstOrDefault().Price;
            var maxPrice = items.OrderByDescending(x => x.Price).FirstOrDefault().Price;
            var featuredExpiry = items.FirstOrDefault(x => x.IsFeatured == true).ExpiryTime;
            var currentItem = 0;

            using (var images = new MagickImageCollection())
            {
                // Add the first image
                var background = new MagickImage(@"Resources\RocketShop\background.jpg");
                images.Add(background);

                var opacityLayer = new MagickImage("xc:rgba(0,0,0,0.7)", new MagickReadSettings()
                {
                    Width = 1920,
                    Height = 1080,
                    BackgroundColor = MagickColors.Transparent
                });
                opacityLayer.ColorSpace = ColorSpace.sRGB;
                images.Add(opacityLayer);

                var title = new MagickImage($"label:TIENDA DIARIA", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Bold.ttf",
                    FontPointsize = 50,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColors.White,
                    Width = 400,
                    Height = 70,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 50
                    }
                });
                title.ColorSpace = ColorSpace.sRGB;
                title.Page = new MagickGeometry($"+475+70");
                images.Add(title);

                var eternalExodusLogo = new MagickImage(@"Resources\RocketShop\EternalExodus.png");
                eternalExodusLogo.Resize(150, 150);
                eternalExodusLogo.Page = new MagickGeometry($"+885+10");
                images.Add(eternalExodusLogo);

                var date = new MagickImage($"label:{DateTime.Today.AddDays(1).Day} {DateTime.Today.ToString("MMMM", CultureInfo.CreateSpecificCulture("es")).ToUpper()}, {DateTime.Today.Year}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Bold.ttf",
                    FontPointsize = 50,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColors.White,
                    Width = 600,
                    Height = 70,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 50
                    }
                });
                date.ColorSpace = ColorSpace.sRGB;
                date.Page = new MagickGeometry($"+1045+70");
                images.Add(date);

                var line1 = new MagickImage("xc:rgb(255,255,255)", new MagickReadSettings()
                {
                    Width = 1400,
                    Height = 4,
                    BackgroundColor = MagickColors.Transparent
                });
                line1.ColorSpace = ColorSpace.sRGB;
                line1.Page = new MagickGeometry($"+260+490");
                images.Add(line1);

                if (items.Where(x => x.IsFeatured == true).Count() == 2)
                {
                    var line2 = new MagickImage("xc:rgb(255,255,255)", new MagickReadSettings()
                    {
                        Width = 4,
                        Height = 150,
                        BackgroundColor = MagickColors.Transparent
                    });
                    line2.ColorSpace = ColorSpace.sRGB;
                    line2.Page = new MagickGeometry($"+960+250");
                    images.Add(line2);

                    var featured1 = CreateItem(items[currentItem++]);
                    featured1.Page = new MagickGeometry($"+275+190");
                    images.Add(featured1);

                    var featured2 = CreateItem(items[currentItem++]);
                    featured2.Page = new MagickGeometry($"+1125+190");
                    images.Add(featured2);
                }
                else
                {
                    var featured1Promo = CreateItem(items[currentItem++]);
                    featured1Promo.Page = new MagickGeometry($"+450+190");
                    images.Add(featured1Promo);
                }

                var daily1 = CreateItem(items[currentItem++]);
                daily1.Page = new MagickGeometry($"+160+525");
                images.Add(daily1);

                var daily2 = CreateItem(items[currentItem++]);
                daily2.Page = new MagickGeometry($"+710+525");
                images.Add(daily2);

                var daily3 = CreateItem(items[currentItem++]);
                daily3.Page = new MagickGeometry($"+1260+525");
                images.Add(daily3);

                var daily4 = CreateItem(items[currentItem++]);
                daily4.Page = new MagickGeometry($"+160+795");
                images.Add(daily4);

                var daily5 = CreateItem(items[currentItem++]);
                daily5.Page = new MagickGeometry($"+710+795");
                images.Add(daily5);

                var daily6 = CreateItem(items[currentItem++]);
                daily6.Page = new MagickGeometry($"+1260+795");
                images.Add(daily6);

                // Create a mosaic from both images
                using (var result = images.Mosaic())
                {
                    MemoryStream ms = new MemoryStream();
                    result.Write(ms);
                    return (ms, minPrice, maxPrice, featuredExpiry);
                }
            }
        }

        private MagickImage CreateItem(RlShopItem item)
        {
            using (var images = new MagickImageCollection())
            {
                string bgName = item.IsFeatured ? string.IsNullOrEmpty(item.ItemPromoImageUrl) ? "featured" : "featuredPromo" : "daily";
                int spacerWidth = item.IsFeatured ? 10 : 0;
                int spacerHeight = item.IsFeatured ? 10 : 0;
                int titleExtraWidthSpacer = item.IsFeatured ? 3 : 0;
                int descSpacer = string.IsNullOrEmpty(item.ItemDesc) ? 0 : 10;

                var itembg = new MagickImage($@"Resources\RocketShop\{bgName}ItemBackground.png");
                itembg.ColorSpace = ColorSpace.sRGB;
                itembg.BackgroundColor = MagickColors.Transparent;
                images.Add(itembg);

                var itemImage = new MagickImage(item.ImagePath);
                itemImage.ColorSpace = ColorSpace.sRGB;
                itemImage.Resize(200, 200);
                itemImage.Page = new MagickGeometry($"+{15 + spacerWidth + titleExtraWidthSpacer}+{25 + spacerHeight}");
                itemImage.ColorFuzz = new Percentage(8);
                itemImage.Opaque(MagickColor.FromRgb(0, 0, 0), MagickColors.Transparent);
                images.Add(itemImage);

                if (!string.IsNullOrEmpty(item.ItemPromoImageUrl))
                {
                    var itemPromoImage = new MagickImage(item.ItemPromoImageUrl);
                    itemPromoImage.ColorSpace = ColorSpace.sRGB;
                    itemPromoImage.Resize(itemPromoImage.Width * 220 / itemPromoImage.Height, 220);
                    itemPromoImage.Page = new MagickGeometry($"+{280 + spacerWidth + titleExtraWidthSpacer}+{15 + spacerHeight}");
                    images.Add(itemPromoImage);
                }

                var itemName = new MagickImage($"label:{item.ItemName}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                    FontWeight = FontWeight.Light,
                    FontPointsize = 30,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColors.White,
                    Width = 400,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                var textWidth = itemName.FontTypeMetrics(item.ItemName).TextWidth;
                var textExtraSpace = textWidth > 240 ? textWidth - 240 : 0;
                itemName.Page = new MagickGeometry($"+{225 + spacerWidth - textExtraSpace}+{30 + spacerHeight}");
                images.Add(itemName);

                if (!string.IsNullOrEmpty(item.ItemDesc))
                {
                    var itemDesc = new MagickImage($"label:{item.ItemDesc}", new MagickReadSettings
                    {
                        Font = @"Resources\Fonts\Montserrat-Regular.ttf",
                        FontPointsize = 25,
                        BackgroundColor = MagickColors.Transparent,
                        FillColor = MagickColors.White,
                        Width = 240,
                        Height = 50,
                        Defines = new CaptionReadDefines()
                        {
                            MaxFontPointsize = 35
                        }
                    });
                    itemDesc.Page = new MagickGeometry($"+{225 + spacerWidth}+{62 + spacerHeight}");
                    images.Add(itemDesc);
                }

                var itemRarity = new MagickImage($"label:{item.ItemRarity}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Regular.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(180, 180, 180),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemRarity.Page = new MagickGeometry($"+{225 + spacerWidth}+{100 + spacerHeight + descSpacer}");
                images.Add(itemRarity);

                var itemType = new MagickImage($"label:{item.ItemType}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Regular.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(180, 180, 180),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemType.Page = new MagickGeometry($"+{225 + spacerWidth}+{130 + spacerHeight + descSpacer}");
                images.Add(itemType);

                var creditsIcon = new MagickImage(@"Resources\RocketShop\credits.png");
                creditsIcon.ColorSpace = ColorSpace.sRGB;
                creditsIcon.BackgroundColor = MagickColors.Transparent;
                creditsIcon.Resize(40, 40);
                creditsIcon.Page = new MagickGeometry($"+{225 + spacerWidth}+{190 + spacerHeight}");
                images.Add(creditsIcon);

                var itemPrice = new MagickImage($"label:{item.Price}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(34, 99, 160),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemPrice.Page = new MagickGeometry($"+{275 + spacerWidth}+{195 + spacerHeight}");
                images.Add(itemPrice);

                var timerIcon = new MagickImage(@"Resources\RocketShop\timericon.webp");
                timerIcon.ColorSpace = ColorSpace.sRGB;
                timerIcon.BackgroundColor = MagickColors.Transparent;
                timerIcon.Resize(40, 40);
                timerIcon.Page = new MagickGeometry($"+{350 + spacerWidth}+{190 + spacerHeight}");
                images.Add(timerIcon);

                var itemTime = new MagickImage($"label:{item.ExpiryTime} {(item.ExpiryTime > 1 ? "Days" : "Day")}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(254, 234, 132),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemTime.Page = new MagickGeometry($"+{390 + spacerWidth}+{195 + spacerHeight}");
                images.Add(itemTime);

                if (!string.IsNullOrEmpty(item.ItemCert))
                {
                    int rectWidth = 60 + item.ItemCert.Length * 7;
                    int positionX = (15 + spacerWidth + titleExtraWidthSpacer + 100) - rectWidth / 2;
                    int positionY = item.ItemColor == PaintsEnum.None ? 185 : 150;

                    var itemCertRect = new MagickImage(MagickColors.Transparent, rectWidth + 20, 55);
                    DrawableStrokeColor strokeColor = new DrawableStrokeColor(MagickColor.FromRgba(220, 220, 220, 255));
                    DrawableStrokeWidth stokeWidth = new DrawableStrokeWidth(3);
                    DrawableFillColor fillColor = new DrawableFillColor(MagickColor.FromRgba(70, 70, 70, 190));
                    DrawableRoundRectangle rectangle = new DrawableRoundRectangle(10, 10, rectWidth, 35, 10, 10);
                    itemCertRect.Draw(strokeColor, stokeWidth, fillColor, rectangle);
                    itemCertRect.Page = new MagickGeometry($"+{positionX}+{positionY}");
                    images.Add(itemCertRect);

                    var itemCertName = new MagickImage($"label:{item.ItemCert}", new MagickReadSettings
                    {
                        Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                        FontPointsize = 17,
                        BackgroundColor = MagickColors.Transparent,
                        FillColor = MagickColor.FromRgba(255, 255, 255, 255),
                        Width = rectWidth,
                        Height = 35,
                        TextGravity = Gravity.Center,
                        Defines = new CaptionReadDefines()
                        {
                            MaxFontPointsize = 15
                        }
                    });
                    itemCertName.Page = new MagickGeometry($"+{5 + positionX}+{5 + positionY}");
                    images.Add(itemCertName);

                }

                if (item.ItemColor != PaintsEnum.None)
                {
                    string paintName = string.Concat(Enum.GetName(typeof(PaintsEnum), item.ItemColor).Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                    int rectWidth = 60 + paintName.Length * 7;
                    int positionX = (15 + spacerWidth + titleExtraWidthSpacer + 100) - rectWidth / 2;
                    int positionY = 185;

                    var paint = RocketLeagueUtils.GetItemPaint(item.ItemColor);
                    var itemPaint = new MagickImage(MagickColors.Transparent, rectWidth + 20, 55);
                    DrawableStrokeColor strokeColor = new DrawableStrokeColor(paint.BorderColor);
                    DrawableStrokeWidth stokeWidth = new DrawableStrokeWidth(3);
                    DrawableFillColor fillColor = new DrawableFillColor(paint.FillColor);
                    DrawableRoundRectangle rectangle = new DrawableRoundRectangle(10, 10, rectWidth, 35, 10, 10);
                    itemPaint.Draw(strokeColor, stokeWidth, fillColor, rectangle);
                    itemPaint.Page = new MagickGeometry($"+{positionX}+{positionY}");
                    images.Add(itemPaint);

                    var itemPaintName = new MagickImage($"label:{paintName}", new MagickReadSettings
                    {
                        Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                        FontPointsize = 17,
                        BackgroundColor = MagickColors.Transparent,
                        FillColor = paint.TextColor,
                        Width = rectWidth,
                        Height = 35,
                        TextGravity = Gravity.Center,
                        Defines = new CaptionReadDefines()
                        {
                            MaxFontPointsize = 15
                        }
                    });
                    itemPaintName.Page = new MagickGeometry($"+{5 + positionX}+{5 + positionY}");
                    images.Add(itemPaintName);
                }

                return images.Mosaic() as MagickImage;
            }
        }

        private List<RlShopItem> GetItems()
        {
            List<RlShopItem> shopItems = new List<RlShopItem>();

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(webUtils.GetPageHtml($"https://rocket-league.com/items/shop"));
            var timerFeatured = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='timer-featured']")?.Attributes.First(x => x.Name == "data-time").Value;
            var timerDaily = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='timer-daily']")?.Attributes.First(x => x.Name == "data-time").Value;
            var featuredNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__featured']").ChildNodes.Where(x => x.Name != "#text").ToList();
            var dailyNodes = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__daily']").ChildNodes.Where(x => x.Name != "#text").ToList();

            foreach (var node in featuredNode)
            {
                HtmlDocument nodeDocument = new HtmlDocument();
                nodeDocument.LoadHtml(node.InnerHtml);

                var itemFullName = nodeDocument.DocumentNode.SelectSingleNode("//h1[@class='rlg-item-shop__name']").InnerText;
                var itemFullCategory = nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__item-category')]").InnerText;

                var itemRarity = RocketLeagueUtils.GetItemRarity(node.Attributes.FirstOrDefault(x => x.Name == "class")?.Value.Split(' ').FirstOrDefault(x => x.StartsWith("is--"))?.Replace("is--", "") ?? "");
                var itemType = itemFullCategory.Replace(itemRarity, "").Trim();
                var imageUrl = nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__item-image')]")?.ChildNodes.FirstOrDefault(x => x.Name == "img")?.Attributes.First(x => x.Name == "src")?.Value;
                var itemEdition = nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__item-edition')]")?.InnerText ?? "";
                var itemNameDesc = RocketLeagueUtils.GetItemNameDesc(itemFullName, itemType, itemEdition);
                var itemPrice = 0;
                Int32.TryParse(nodeDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__item-credits']").InnerText.Replace("\n", "").Trim(), out itemPrice);
                PaintsEnum itemColor = PaintsEnum.None;
                Enum.TryParse(nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__paint')]")?.InnerText.Replace(" ", "").Replace("\n", "").Trim(), out itemColor);
                var itemCert = nodeDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__cert']")?.InnerText.Replace("\n", "").Trim() ?? "";
                var itemPromoImageUrl = nodeDocument.DocumentNode.SelectSingleNode("//img[contains(@class, 'rlg-itemshop__promo')]")?.Attributes.FirstOrDefault(x => x.Name == "src")?.Value;

                shopItems.Add(new RlShopItem()
                {
                    ItemName = itemNameDesc.Name,
                    ItemDesc = itemNameDesc.Desc,
                    ItemRarity = itemRarity,
                    ItemType = itemType,
                    ItemCert = itemCert,
                    ItemColor = itemColor,
                    ImagePath = @"https://rocket-league.com/" + imageUrl,
                    Price = itemPrice,
                    IsFeatured = true,
                    ExpiryTime = Convert.ToInt32(Math.Ceiling((Convert.ToDateTime(timerFeatured) - DateTime.Now).TotalDays)),
                    ItemPromoImageUrl = string.IsNullOrEmpty(itemPromoImageUrl) ? null : @"https://rocket-league.com/" + itemPromoImageUrl
                });
            }

            foreach (var node in dailyNodes)
            {
                HtmlDocument nodeDocument = new HtmlDocument();
                nodeDocument.LoadHtml(node.InnerHtml);

                var itemFullName = nodeDocument.DocumentNode.SelectSingleNode("//h1[@class='rlg-h2 rlg-item-shop__name --daily']").InnerText;
                var itemFullCategory = nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__item-category --daily')]").InnerText;

                var itemRarity = RocketLeagueUtils.GetItemRarity(node.Attributes.FirstOrDefault(x => x.Name == "class")?.Value.Split(' ').FirstOrDefault(x => x.StartsWith("is--"))?.Replace("is--", "") ?? "");
                var itemType = itemFullCategory.Replace(itemRarity, "").Trim();
                var imageUrl = nodeDocument.DocumentNode.SelectSingleNode("//img[@class='rlg-item-shop__item-image --daily']").Attributes.First(x => x.Name == "src")?.Value;
                var itemNameDesc = RocketLeagueUtils.GetItemNameDesc(itemFullName, itemType);
                var itemPrice = 0;
                Int32.TryParse(nodeDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__item-credits --daily']").InnerText.Replace("\n", "").Trim(), out itemPrice);
                PaintsEnum itemColor = PaintsEnum.None;
                Enum.TryParse(nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__image-meta --daily')]")?.ChildNodes.FirstOrDefault(x => x.Name == "div")?.InnerText.Replace(" ", ""), out itemColor);
                var itemCert = nodeDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__cert --daily']")?.InnerText.Replace("\n", "").Trim() ?? "";

                shopItems.Add(new RlShopItem()
                {
                    ItemName = itemNameDesc.Name,
                    ItemDesc = itemNameDesc.Desc,
                    ItemRarity = itemRarity,
                    ItemType = itemType,
                    ItemCert = itemCert,
                    ItemColor = itemColor,
                    ImagePath = @"https://rocket-league.com/" + imageUrl,
                    Price = itemPrice,
                    IsFeatured = false,
                    ExpiryTime = Convert.ToInt32(Math.Ceiling((Convert.ToDateTime(timerDaily) - DateTime.Now).TotalDays))
                });
            }

            return shopItems;
        }
    }
}
