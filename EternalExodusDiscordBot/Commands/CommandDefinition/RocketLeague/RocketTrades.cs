﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague
{
    public class RocketTrades
    {
        public Embed GetTrade()
        {
            WebResponse response;
            Stream dataStream;
            StreamReader reader;
            string responseFromServer;

            WebRequest request = (HttpWebRequest)WebRequest.Create($"https://rocket-league.com/trading?p=1");

            response = request.GetResponse();
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(responseFromServer);
            var nodes = htmlDocument.DocumentNode.SelectNodes("//div[@class='rlg-trade-display-container is--user']");
            List<TradeOffer> tradeOffers = new List<TradeOffer>();

            foreach (var node in nodes)
            {
                var tradeHeader = GetTradeHeader(node);
                var tradeItems = GetTradeItems(node);
                var platformLink = GetPlatformLink(node);

                tradeOffers.Add(new TradeOffer
                {
                    Username = tradeHeader.Username,
                    Platform = tradeHeader.Platform,
                    TradeLink = tradeHeader.TradeLink,
                    ProfileLink = tradeHeader.ProfileLink,
                    PlatformLink = platformLink,
                    TradeDate = tradeHeader.TradeDate,
                    ItemList = tradeItems,
                    TradeExpiration = DateTime.Now.AddDays(15)
                });

            }

            var embedTrade = tradeOffers.First();
            string haveItems = "";
            string wantItems = "";

            foreach (var item in embedTrade.ItemList.Where(x => x.HaveWant == 0))
            {
                haveItems += "x" + item.Ammount + " " + item.Item.itemName + " " + item.Paint + " " + item.Certificate + Environment.NewLine;
            }

            foreach (var item in embedTrade.ItemList.Where(x => x.HaveWant == 1))
            {
                wantItems += "x" + item.Ammount + " " + item.Item.itemName + " " + item.Paint + " " + item.Certificate + Environment.NewLine;
            }

            EmbedFieldBuilder headerField = new EmbedFieldBuilder()
                .WithName("Trade Info")
                .WithValue("Username: " + embedTrade.Username + Environment.NewLine +
                                "Platform: " + embedTrade.Platform + Environment.NewLine +
                                "Trade link: " + embedTrade.TradeLink + Environment.NewLine +
                                "Profile link: " + embedTrade.ProfileLink + Environment.NewLine +
                                "Platform link: " + embedTrade.PlatformLink + Environment.NewLine +
                                "Trade date: " + embedTrade.TradeDate);
            EmbedFieldBuilder haveItemsField = new EmbedFieldBuilder()
                .WithName("Have")
                .WithValue(haveItems);

            EmbedFieldBuilder wantItemsField = new EmbedFieldBuilder()
                .WithName("Want")
                .WithValue(wantItems);

            Embed textoEmbed = new EmbedBuilder()
                .WithColor(new Discord.Color(26, 255, 163))
                .WithAuthor("Trade Offer Alpha 1.1")
                .AddField(headerField)
                .AddField(haveItemsField)
                .AddField(wantItemsField)
                .WithCurrentTimestamp()
                //.WithThumbnailUrl(userImg)
                .Build();

            return textoEmbed;
        }

        private TradeHeader GetTradeHeader(HtmlNode htmlNode)
        {
            var headerNode = htmlNode.SelectSingleNode("//div[@class='rlg-trade-display-header']");
            var tradeLink = "https://rocket-league.com" + headerNode.ChildNodes.First(x => x.Name.Equals("a")).Attributes.First(x => x.Name.Equals("href")).Value;
            var platformUsername = headerNode.SelectSingleNode("//div[@class='rlg-trade-platform-name']").ChildNodes.First(x => x.Name.Equals("span")).InnerText;
            var platform = platformUsername.Split(':')[0].Trim();
            var username = platformUsername.Split(':')[1].Trim();
            var tradeDate = headerNode.SelectSingleNode("//span[@class='rlg-trade-display-added']").InnerText.Split('.')[0].Replace("\n", "");
            var profileLink = "https://rocket-league.com" + headerNode.SelectSingleNode("//a[@class='rlg-trade-player-link']").Attributes.First(x => x.Name.Equals("href")).Value;

            return new TradeHeader
            {
                Username = username,
                Platform = GetPlatform(platform),
                TradeLink = tradeLink,
                ProfileLink = profileLink,
                TradeDate = tradeDate
            };
        }

        private List<ItemOffer> GetTradeItems(HtmlNode htmlNode)
        {
            List<ItemOffer> itemOffers = new List<ItemOffer>();

            var itemsNode = htmlNode.SelectSingleNode("//div[@class='rlg-trade-display-items']");
            var haveItemsNode = itemsNode.SelectSingleNode("//div[@id='rlg-youritems']").ChildNodes.Where(x => x.Name.Equals("a")).ToList();
            var wantItemsNode = itemsNode.SelectSingleNode("//div[@id='rlg-theiritems']").ChildNodes.Where(x => x.Name.Equals("a")).ToList();

            foreach (var node in haveItemsNode)
            {
                itemOffers.Add(GetItemOffer(node, 0));
            }

            foreach (var node in wantItemsNode)
            {
                itemOffers.Add(GetItemOffer(node, 1));
            }

            return itemOffers;
        }

        private ItemOffer GetItemOffer(HtmlNode htmlNode, int haveWant)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlNode.InnerHtml);
            var item = doc.DocumentNode.SelectSingleNode("//h2").InnerText;
            var cert = doc.DocumentNode.SelectSingleNode("//span")?.InnerText;
            var paint = doc.DocumentNode.SelectSingleNode("//div[@class='rlg-trade-display-item-paint']")?.Attributes.First(x => x.Name.Equals("data-name")).Value;
            var ammount = doc.DocumentNode.SelectSingleNode("//div[@class='rlg-trade-display-item__amount is--premium wide']")?.InnerText.Trim();

            return new ItemOffer()
            {
                Item = new Item
                {
                    itemName = item,
                    itemPhoto = null
                },
                Certificate = cert,
                Paint = paint,
                HaveWant = haveWant,
                Ammount = !string.IsNullOrEmpty(ammount) ? Convert.ToInt32(ammount) : 1
            };
        }

        private string GetPlatformLink(HtmlNode htmlNode)
        {
            return htmlNode.SelectSingleNode("//div[@class='rlg-trade-link-container']").ChildNodes.First(x => x.Name.Equals("div")).ChildNodes.First(x => x.Name.Equals("div")).ChildNodes.First(x => x.Name.Equals("p")).ChildNodes.Where(x => x.Name.Equals("a")).ToList()?[1]?.Attributes.First(x => x.Name.Equals("href")).Value;
        }

        private PlatformEnum GetPlatform(string platform)
        {
            switch (platform)
            {
                case "Steam":
                    return PlatformEnum.steam;
                case "PSN ID":
                    return PlatformEnum.psn;
                case "Switch":
                    return PlatformEnum.nswitch;
                case "XBOX Gamertag":
                    return PlatformEnum.xbl;
                default:
                    return PlatformEnum.unknown;
            }
        }
    }
}
