﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.Utils;
using HtmlAgilityPack;
using ImageMagick;
using ImageMagick.Formats.Caption;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague
{
    public class RocketEsportsShop
    {
        private WebUtils webUtils = new WebUtils();

        #region Deprecated Methods

        /// <summary>
        /// Old Twitter RL Eshop Method
        /// </summary>
        /// <returns></returns>
        //public async Task<Embed> GetRocketEShop()
        //{
        //    if (!File.Exists("Resources/RocketShop/eshop.jpg"))
        //    {
        //        TwitterUtils twUtils = new TwitterUtils();
        //        await twUtils.GetRlEsportsShop();
        //    }

        //    if (File.Exists("Resources/RocketShop/eshop.jpg"))
        //    {
        //        WebUtils webUtils = new WebUtils();
        //        Embed embed = new EmbedBuilder()
        //        .WithColor(new Discord.Color(237, 19, 128))
        //        .WithTitle($"**{new Emoji(":shopping_cart:")} | TIENDA DIARIA ESPORTS - ROCKET LEAGUE**")
        //        .WithImageUrl(await webUtils.UploadImageToSul("Resources/RocketShop/eshop.jpg"))
        //        .Build();

        //        return embed;
        //    }
        //    else
        //    {
        //        EmbedUtils embedUtils = new EmbedUtils();
        //        return embedUtils.ErrorEmbed("No se ha podido obtener la tienda de esports.");
        //    }
        //}

        #endregion

        public string GetRocketEShop()
        {
            var shopImage = GenerateEsportsShopImage();
            string shopImagePath = "Resources/RocketShop/eshop.png";

            using (FileStream fs = new FileStream(shopImagePath, FileMode.Create))
            {
                shopImage.image.WriteTo(fs);
                fs.Flush();
            }

            return shopImagePath;
        }

        private (MemoryStream image, int minPrice, int maxPrice, int featuredExpiry) GenerateEsportsShopImage()
        {
            var items = GetItems();
            var minPrice = items.OrderBy(x => x.Price).FirstOrDefault().Price;
            var maxPrice = items.OrderByDescending(x => x.Price).FirstOrDefault().Price;
            var featuredExpiry = 1;
            var currentItem = 0;

            using (var images = new MagickImageCollection())
            {
                // Add the first image
                var background = new MagickImage(@"Resources\RocketShop\backgroundesports.jpg");
                images.Add(background);

                var opacityLayer = new MagickImage("xc:rgba(0,0,0,0.7)", new MagickReadSettings()
                {
                    Width = 1920,
                    Height = 1080,
                    BackgroundColor = MagickColors.Transparent
                });
                opacityLayer.ColorSpace = ColorSpace.sRGB;
                images.Add(opacityLayer);

                var title = new MagickImage($"label:TIENDA DIARIA ESPORTS", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Bold.ttf",
                    FontPointsize = 50,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColors.White,
                    Width = 660,
                    Height = 70,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 50
                    }
                });
                title.ColorSpace = ColorSpace.sRGB;
                title.Page = new MagickGeometry($"+215+70");
                images.Add(title);

                var eternalExodusLogo = new MagickImage(@"Resources\RocketShop\EternalExodus.png");
                eternalExodusLogo.Resize(150, 150);
                eternalExodusLogo.Page = new MagickGeometry($"+885+10");
                images.Add(eternalExodusLogo);

                var date = new MagickImage($"label:{DateTime.Today.AddDays(1).Day} {DateTime.Today.ToString("MMMM", CultureInfo.CreateSpecificCulture("es")).ToUpper()}, {DateTime.Today.Year}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Bold.ttf",
                    FontPointsize = 50,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColors.White,
                    Width = 600,
                    Height = 70,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 50
                    }
                });
                date.ColorSpace = ColorSpace.sRGB;
                date.Page = new MagickGeometry($"+1045+70");
                images.Add(date);

                var item1 = CreateItem(items[currentItem++]);
                item1.Page = new MagickGeometry($"+160+325");
                images.Add(item1);

                var item2 = CreateItem(items[currentItem++]);
                item2.Page = new MagickGeometry($"+710+325");
                images.Add(item2);

                var item3 = CreateItem(items[currentItem++]);
                item3.Page = new MagickGeometry($"+1260+325");
                images.Add(item3);

                var item4 = CreateItem(items[currentItem++]);
                item4.Page = new MagickGeometry($"+160+665");
                images.Add(item4);

                var item5 = CreateItem(items[currentItem++]);
                item5.Page = new MagickGeometry($"+710+665");
                images.Add(item5);

                var item6 = CreateItem(items[currentItem++]);
                item6.Page = new MagickGeometry($"+1260+665");
                images.Add(item6);

                // Create a mosaic from both images
                using (var result = images.Mosaic())
                {
                    MemoryStream ms = new MemoryStream();
                    result.Write(ms);
                    return (ms, minPrice, maxPrice, featuredExpiry);
                }
            }
        }

        private MagickImage CreateItem(RlShopItem item)
        {
            using (var images = new MagickImageCollection())
            {
                string bgName = item.IsFeatured ? string.IsNullOrEmpty(item.ItemPromoImageUrl) ? "featured" : "featuredPromo" : "daily";
                int spacerWidth = item.IsFeatured ? 10 : 0;
                int spacerHeight = item.IsFeatured ? 10 : 0;
                int titleExtraWidthSpacer = item.IsFeatured ? 3 : 0;
                int descSpacer = string.IsNullOrEmpty(item.ItemDesc) ? 0 : 10;

                var itembg = new MagickImage($@"Resources\RocketShop\{bgName}ItemBackground.png");
                itembg.ColorSpace = ColorSpace.sRGB;
                itembg.BackgroundColor = MagickColors.Transparent;
                images.Add(itembg);

                var itemImage = new MagickImage(item.ImagePath);
                itemImage.ColorSpace = ColorSpace.sRGB;
                itemImage.Resize(200, 200);
                itemImage.Page = new MagickGeometry($"+{15 + spacerWidth + titleExtraWidthSpacer}+{25 + spacerHeight}");
                itemImage.ColorFuzz = new Percentage(8);
                itemImage.Opaque(MagickColor.FromRgb(0, 0, 0), MagickColors.Transparent);
                images.Add(itemImage);

                //if (!string.IsNullOrEmpty(item.ItemPromoImageUrl))
                //{
                //    var itemPromoImage = new MagickImage(item.ItemPromoImageUrl);
                //    itemPromoImage.ColorSpace = ColorSpace.sRGB;
                //    itemPromoImage.Resize(itemPromoImage.Width * 220 / itemPromoImage.Height, 220);
                //    itemPromoImage.Page = new MagickGeometry($"+{280 + spacerWidth + titleExtraWidthSpacer}+{15 + spacerHeight}");
                //    images.Add(itemPromoImage);
                //}

                var itemName = new MagickImage($"label:{item.ItemName}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                    FontWeight = FontWeight.Light,
                    FontPointsize = 30,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColors.White,
                    Width = 400,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                var textWidth = itemName.FontTypeMetrics(item.ItemName).TextWidth;
                var textExtraSpace = textWidth > 240 ? textWidth - 240 : 0;
                itemName.Page = new MagickGeometry($"+{225 + spacerWidth - textExtraSpace}+{30 + spacerHeight}");
                images.Add(itemName);

                if (!string.IsNullOrEmpty(item.ItemDesc))
                {
                    var itemDesc = new MagickImage($"label:{item.ItemDesc}", new MagickReadSettings
                    {
                        Font = @"Resources\Fonts\Montserrat-Regular.ttf",
                        FontPointsize = 25,
                        BackgroundColor = MagickColors.Transparent,
                        FillColor = MagickColors.White,
                        Width = 240,
                        Height = 50,
                        Defines = new CaptionReadDefines()
                        {
                            MaxFontPointsize = 35
                        }
                    });
                    itemDesc.Page = new MagickGeometry($"+{225 + spacerWidth}+{62 + spacerHeight}");
                    images.Add(itemDesc);
                }

                var itemRarity = new MagickImage($"label:{item.ItemRarity}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Regular.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(180, 180, 180),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemRarity.Page = new MagickGeometry($"+{225 + spacerWidth}+{100 + spacerHeight + descSpacer}");
                images.Add(itemRarity);

                var itemType = new MagickImage($"label:{item.ItemType}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-Regular.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(180, 180, 180),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemType.Page = new MagickGeometry($"+{225 + spacerWidth}+{130 + spacerHeight + descSpacer}");
                images.Add(itemType);

                var tokensIcon = new MagickImage(@"Resources\RocketShop\tokens.png");
                tokensIcon.ColorSpace = ColorSpace.sRGB;
                tokensIcon.BackgroundColor = MagickColors.Transparent;
                tokensIcon.Resize(40, 40);
                tokensIcon.Page = new MagickGeometry($"+{225 + spacerWidth}+{190 + spacerHeight}");
                images.Add(tokensIcon);

                var itemPrice = new MagickImage($"label:{item.Price}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(188, 72, 222),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemPrice.Page = new MagickGeometry($"+{275 + spacerWidth}+{195 + spacerHeight}");
                images.Add(itemPrice);

                var timerIcon = new MagickImage(@"Resources\RocketShop\timericon.webp");
                timerIcon.ColorSpace = ColorSpace.sRGB;
                timerIcon.BackgroundColor = MagickColors.Transparent;
                timerIcon.Resize(40, 40);
                timerIcon.Page = new MagickGeometry($"+{350 + spacerWidth}+{190 + spacerHeight}");
                images.Add(timerIcon);

                var itemTime = new MagickImage($"label:{item.ExpiryTime} {(item.ExpiryTime > 1 ? "Days" : "Day")}", new MagickReadSettings
                {
                    Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                    FontPointsize = 25,
                    BackgroundColor = MagickColors.Transparent,
                    FillColor = MagickColor.FromRgb(254, 234, 132),
                    Width = 240,
                    Height = 50,
                    Defines = new CaptionReadDefines()
                    {
                        MaxFontPointsize = 35
                    }
                });
                itemTime.Page = new MagickGeometry($"+{390 + spacerWidth}+{195 + spacerHeight}");
                images.Add(itemTime);

                //if (!string.IsNullOrEmpty(item.ItemCert))
                //{
                //    int rectWidth = 60 + item.ItemCert.Length * 7;
                //    int positionX = (15 + spacerWidth + titleExtraWidthSpacer + 100) - rectWidth / 2;
                //    int positionY = item.ItemColor == PaintsEnum.None ? 185 : 150;

                //    var itemCertRect = new MagickImage(MagickColors.Transparent, rectWidth + 20, 55);
                //    DrawableStrokeColor strokeColor = new DrawableStrokeColor(MagickColor.FromRgba(220, 220, 220, 255));
                //    DrawableStrokeWidth stokeWidth = new DrawableStrokeWidth(3);
                //    DrawableFillColor fillColor = new DrawableFillColor(MagickColor.FromRgba(70, 70, 70, 190));
                //    DrawableRoundRectangle rectangle = new DrawableRoundRectangle(10, 10, rectWidth, 35, 10, 10);
                //    itemCertRect.Draw(strokeColor, stokeWidth, fillColor, rectangle);
                //    itemCertRect.Page = new MagickGeometry($"+{positionX}+{positionY}");
                //    images.Add(itemCertRect);

                //    var itemCertName = new MagickImage($"label:{item.ItemCert}", new MagickReadSettings
                //    {
                //        Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                //        FontPointsize = 17,
                //        BackgroundColor = MagickColors.Transparent,
                //        FillColor = MagickColor.FromRgba(255, 255, 255, 255),
                //        Width = rectWidth,
                //        Height = 35,
                //        TextGravity = Gravity.Center,
                //        Defines = new CaptionReadDefines()
                //        {
                //            MaxFontPointsize = 15
                //        }
                //    });
                //    itemCertName.Page = new MagickGeometry($"+{5 + positionX}+{5 + positionY}");
                //    images.Add(itemCertName);

                //}

                if (item.ItemColor != PaintsEnum.None)
                {
                    string paintName = string.Concat(Enum.GetName(typeof(PaintsEnum), item.ItemColor).Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                    int rectWidth = 60 + paintName.Length * 7;
                    int positionX = (15 + spacerWidth + titleExtraWidthSpacer + 100) - rectWidth / 2;
                    int positionY = 185;

                    var paint = RocketLeagueUtils.GetItemPaint(item.ItemColor);
                    var itemPaint = new MagickImage(MagickColors.Transparent, rectWidth + 20, 55);
                    DrawableStrokeColor strokeColor = new DrawableStrokeColor(paint.BorderColor);
                    DrawableStrokeWidth stokeWidth = new DrawableStrokeWidth(3);
                    DrawableFillColor fillColor = new DrawableFillColor(paint.FillColor);
                    DrawableRoundRectangle rectangle = new DrawableRoundRectangle(10, 10, rectWidth, 35, 10, 10);
                    itemPaint.Draw(strokeColor, stokeWidth, fillColor, rectangle);
                    itemPaint.Page = new MagickGeometry($"+{positionX}+{positionY}");
                    images.Add(itemPaint);

                    var itemPaintName = new MagickImage($"label:{paintName}", new MagickReadSettings
                    {
                        Font = @"Resources\Fonts\Montserrat-SemiBold.ttf",
                        FontPointsize = 17,
                        BackgroundColor = MagickColors.Transparent,
                        FillColor = paint.TextColor,
                        Width = rectWidth,
                        Height = 35,
                        TextGravity = Gravity.Center,
                        Defines = new CaptionReadDefines()
                        {
                            MaxFontPointsize = 15
                        }
                    });
                    itemPaintName.Page = new MagickGeometry($"+{5 + positionX}+{5 + positionY}");
                    images.Add(itemPaintName);
                }

                return images.Mosaic() as MagickImage;
            }
        }

        private List<RlShopItem> GetItems()
        {
            List<RlShopItem> shopItems = new List<RlShopItem>();

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(webUtils.GetPageHtml($"https://rocket-league.com/items/shop/esports"));
            var esportsNodes = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__daily rlg-item-shop__esports']").ChildNodes.Where(x => x.Name != "#text").ToList();

            foreach (var node in esportsNodes)
            {
                HtmlDocument nodeDocument = new HtmlDocument();
                nodeDocument.LoadHtml(node.InnerHtml);

                var itemFullName = nodeDocument.DocumentNode.SelectSingleNode("//h1[@class='rlg-h2 rlg-item-shop__name --daily']").InnerText;
                var itemFullCategory = nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__item-category --daily')]").InnerText;

                var itemRarity = RocketLeagueUtils.GetItemRarity(node.Attributes.FirstOrDefault(x => x.Name == "class")?.Value.Split(' ').FirstOrDefault(x => x.StartsWith("is--"))?.Replace("is--", "") ?? "");
                var itemType = itemFullCategory.Replace(itemRarity, "").Trim();
                var imageUrl = nodeDocument.DocumentNode.SelectSingleNode("//img[@class='rlg-item-shop__item-image --daily']").Attributes.First(x => x.Name == "src")?.Value;
                var itemNameDesc = RocketLeagueUtils.GetItemNameDesc(itemFullName, itemType);
                var itemPrice = 0;
                Int32.TryParse(nodeDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__item-credits rlg-item-shop__item-tokens --daily']").InnerText.Replace("\n", "").Trim(), out itemPrice);
                PaintsEnum itemColor = PaintsEnum.None;
                Enum.TryParse(nodeDocument.DocumentNode.SelectSingleNode("//div[contains(@class, 'rlg-item-shop__image-meta --daily')]")?.ChildNodes.FirstOrDefault(x => x.Name == "div")?.InnerText.Replace(" ", ""), out itemColor);
                //var itemCert = nodeDocument.DocumentNode.SelectSingleNode("//div[@class='rlg-item-shop__cert --daily']")?.InnerText.Replace("\n", "").Trim() ?? "";
                var timer = nodeDocument.DocumentNode.SelectSingleNode($"//div[@id='timer-esports-{esportsNodes.IndexOf(node) + 1}']")?.Attributes.First(x => x.Name == "data-time").Value;

                shopItems.Add(new RlShopItem()
                {
                    ItemName = itemNameDesc.Name,
                    ItemDesc = itemNameDesc.Desc,
                    ItemRarity = itemRarity,
                    ItemType = itemType,
                    //ItemCert = itemCert,
                    ItemColor = itemColor,
                    ImagePath = @"https://rocket-league.com/" + imageUrl,
                    Price = itemPrice,
                    IsFeatured = false,
                    ExpiryTime = Convert.ToInt32(Math.Ceiling((Convert.ToDateTime(timer) - DateTime.Now).TotalDays)),
                });
            }

            return shopItems;
        }
    }
}
