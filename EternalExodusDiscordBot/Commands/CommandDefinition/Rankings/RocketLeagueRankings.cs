﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Rankings
{
    public class RocketLeagueRankings
    {
        public Embed UpdateEmbedRanking(IEmbed oldEmbed, List<RankingsRocketLeague> players, int mode, int page)
        {
            int position = 1;

            List<string> rankings = new List<string>();
            players.ForEach(x => rankings.Add($"{(position++) + page} | {RocketLeagueUtils.GetRankEmoji((int)x.GetType().GetProperty(Enum.GetName(typeof(RocketLeagueModesEnum), mode)).GetValue(x), (RocketLeagueModesEnum)mode)} {x.Username} - {x.GetType().GetProperty(Enum.GetName(typeof(RocketLeagueModesEnum), mode)).GetValue(x)}"));

            if (page == 0)
            {
                rankings[0] = $"**{rankings[0]}**";
                if (rankings.Count > 3)
                {
                    rankings[1] = $"**{rankings[1]}**";
                    rankings[2] = $"**{rankings[2]}**";
                }
            }

            List<EmbedFieldBuilder> embedFieldBuilders = new List<EmbedFieldBuilder>();

            EmbedFieldBuilder col1Field = new EmbedFieldBuilder()
                .WithName("\u200B")
                .WithValue(String.Join(Environment.NewLine, rankings.Take(Convert.ToInt32(decimal.Ceiling((decimal)rankings.Count / 2))).ToArray()))
                .WithIsInline(true);
            embedFieldBuilders.Add(col1Field);

            if (rankings.Count >= 2)
            {
                EmbedFieldBuilder col2Field = new EmbedFieldBuilder()
                .WithName("\u200B")
                .WithValue(String.Join(Environment.NewLine, rankings.Skip(Convert.ToInt32(decimal.Ceiling((decimal)rankings.Count / 2))).Take(rankings.Count - (Convert.ToInt32(decimal.Ceiling((decimal)rankings.Count / 2)))).ToArray()))
                .WithIsInline(true);
                embedFieldBuilders.Add(col2Field);
            }

            string title = oldEmbed.ToEmbedBuilder().Title;

            Embed newEmbed = new EmbedBuilder()
                .WithColor(new Discord.Color(254, 254, 254))
                .WithTitle(title)
                .WithFields(embedFieldBuilders)
                .WithFooter($"Última actualización: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} (Actualización cada {BotSettings.Default.RankingsUpdateTime} minutos)")
                .Build();

            return newEmbed;
        }
    }
}
