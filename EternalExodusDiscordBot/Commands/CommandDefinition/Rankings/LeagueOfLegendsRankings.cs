﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using NLog;
using RiotSharp;
using RiotSharp.Misc;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Rankings
{
    public class LeagueOfLegendsRankings
    {
        private Logger log = LogManager.GetCurrentClassLogger();

        public async Task UpdateSummonerStats(RankingsLeagueOfLegends summonerDb)
        {
            try
            {
                var api = RiotApi.GetDevelopmentInstance(BotSettings.Default.LOLApiKey);
                var leagues = await api.League.GetLeagueEntriesBySummonerAsync((Region)summonerDb.Region, summonerDb.SummonerId);

                foreach (var league in leagues)
                {
                    if (league.QueueType == "RANKED_FLEX_SR") //Flex
                    {
                        summonerDb.TierFlex = league.Tier;
                        summonerDb.RankFlex = league.Rank;
                        summonerDb.LpFlex = league.LeaguePoints;
                    }
                    else if (league.QueueType == "RANKED_SOLO_5x5") //Solo
                    {
                        summonerDb.TierSolo = league.Tier;
                        summonerDb.RankSolo = league.Rank;
                        summonerDb.LpSolo = league.LeaguePoints;
                    }
                }

                using (var db = new EternalExodusEntities())
                {
                    db.RankingsLeagueOfLegends.AddOrUpdate(summonerDb);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error haciendo update en los ranks de lol para el discord ID {summonerDb.DiscordId}, exception: {ex.Message}");
            }
        }

        public List<RankingsLeagueOfLegends> OrderPlayers(List<RankingsLeagueOfLegends> playersList, int gameMode)
        {
            List<(RankingsLeagueOfLegends player, int points)> playersPointsList = new List<(RankingsLeagueOfLegends player, int points)>();

            foreach (var player in playersList)
            {
                int playerPoints = LeagueOfLegendsUtils.GetRankPoints(player.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsTiersEnum), gameMode)).GetValue(player), player.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsRanksEnum), gameMode)).GetValue(player), player.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsLpEnum), gameMode)).GetValue(player));
                playersPointsList.Add((player, playerPoints));
            }

            return playersPointsList.OrderByDescending(x => x.points).Select(x => x.player).ToList();
        }

        public Embed UpdateEmbedRanking(DiscordSocketClient client, IEmbed oldEmbed, List<RankingsLeagueOfLegends> players, int mode)
        {
            int position = 1;

            List<string> rankings = new List<string>();
            players.ForEach(x => rankings.Add($"{position++} | {EmoteUtils.GetEmoteStringByName(LeagueOfLegendsUtils.GetTierEmoji(x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsTiersEnum), mode)).GetValue(x)))} {client.GetUser((ulong)x.DiscordId)?.Username ?? $"DiscID: {x.DiscordId}"} - {x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsTiersEnum), mode)).GetValue(x) ?? "UNRANKED"} {x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsRanksEnum), mode)).GetValue(x) ?? "-"} {x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsLpEnum), mode)).GetValue(x) ?? "0"}LP"));

            rankings[0] = $"**{rankings[0]}**";
            if (rankings.Count > 3)
            {
                rankings[1] = $"**{rankings[1]}**";
                rankings[2] = $"**{rankings[2]}**";
            }

            List<EmbedFieldBuilder> embedFieldBuilders = new List<EmbedFieldBuilder>();

            EmbedFieldBuilder col1Field = new EmbedFieldBuilder()
                .WithName("\u200B")
                .WithValue(String.Join(Environment.NewLine, rankings.Take(Convert.ToInt32(decimal.Ceiling((decimal)rankings.Count / 2))).ToArray()))
                .WithIsInline(true);
            embedFieldBuilders.Add(col1Field);

            if (rankings.Count >= 2)
            {
                EmbedFieldBuilder col2Field = new EmbedFieldBuilder()
                .WithName("\u200B")
                .WithValue(String.Join(Environment.NewLine, rankings.Skip(Convert.ToInt32(decimal.Ceiling((decimal)rankings.Count / 2))).Take(rankings.Count - (Convert.ToInt32(decimal.Ceiling((decimal)rankings.Count / 2)))).ToArray()))
                .WithIsInline(true);
                embedFieldBuilders.Add(col2Field);
            }
            
            string title = oldEmbed.ToEmbedBuilder().Title;

            Embed newEmbed = new EmbedBuilder()
                .WithColor(new Discord.Color(254, 254, 254))
                .WithTitle(title)
                .WithFields(embedFieldBuilders)
                .WithFooter($"Última actualización: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} (Actualización cada {BotSettings.Default.RankingsUpdateTime} minutos)")
                .Build();

            return newEmbed;
        }
    }
}
