﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.CommandDefinition.Rankings
{
    public class MainRanking
    {
        public Embed UpdateMainRanking(DiscordSocketClient client)
        {
            using (var db = new EternalExodusEntities())
            {
                List<EmbedFieldBuilder> embedFieldBuilders = new List<EmbedFieldBuilder>();
                LeagueOfLegendsRankings lolRanks = new LeagueOfLegendsRankings();

                var infoField = new EmbedFieldBuilder()
                .WithName($"**{EmoteUtils.GetEmoteStringByName("Info")} | INFO**")
                .WithValue("Reacciona al emoji de cada juego en el que quieras que aparezca tus estadísticas, tendrás que rellenar un formulario y recuerda que se actualiza cada 15 minutos. Puedes anularlo cuando quieras, reaccionando al emoji nuevamente.");
                embedFieldBuilders.Add(infoField);

                var rltop3 = db.RankingsRocketLeague.OrderByDescending(x => x.v3).Take(3).ToList();
                if (rltop3.Count > 0)
                {
                    int rlPos = 1;
                    string rlValue = "";
                    rltop3.ForEach(x => rlValue += $"**{rlPos++}. {RocketLeagueUtils.GetRankEmoji(x.v3.HasValue ? x.v3.Value : 0, RocketLeagueModesEnum.v3)} {x.Username} - {x.v3}**" + Environment.NewLine);
                    var rlField = new EmbedFieldBuilder()
                    .WithName($"**{EmoteUtils.GetEmoteStringByName("RocketLeague")} | ROCKET LEAGUE TOP 3 (3v3)**")
                    .WithValue(rlValue);
                    embedFieldBuilders.Add(rlField);
                }

                var loltop3 = lolRanks.OrderPlayers(db.RankingsLeagueOfLegends.ToList(), 1010).Take(3).ToList();
                if (loltop3.Count > 0)
                {
                    int lolPos = 1;
                    string lolValue = "";
                    loltop3.ForEach(x => lolValue += $"**{lolPos++}. {EmoteUtils.GetEmoteStringByName(LeagueOfLegendsUtils.GetTierEmoji(x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsTiersEnum), 1010)).GetValue(x)))} {client.GetUser((ulong)x.DiscordId).Username} - {x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsTiersEnum), 1010)).GetValue(x) ?? "UNRANKED"} {x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsRanksEnum), 1010)).GetValue(x) ?? "-"} {x.GetType().GetProperty(Enum.GetName(typeof(LeagueOfLegendsLpEnum), 1010)).GetValue(x) ?? "0"}LP**" + Environment.NewLine);
                    var lolField = new EmbedFieldBuilder()
                    .WithName($"**{EmoteUtils.GetEmoteStringByName("LeagueOfLegends")} | LEAGUE OF LEGENDS TOP 3 (SOLO/DUO)**")
                    .WithValue(lolValue);
                    embedFieldBuilders.Add(lolField);
                }


                var mainEmbed = new EmbedBuilder()
                    .WithTitle("**🥇 | RANKING GLOBAL - ETERNAL EXODUS**")
                    .WithFields(embedFieldBuilders)
                    .WithFooter($"Última actualización: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} (Actualización cada {BotSettings.Default.RankingsUpdateTime} minutos) | Embed type: 3")
                    .WithColor(255, 255, 0)
                    .Build();

                return mainEmbed;
            }
        }
    }
}
