﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.CommandDefinition.Forms;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands
{
    public class HandlerRemovedReactions
    {
        private DiscordSocketClient _client;
        private EmbedUtils embedUtils;
        private Logger log = LogManager.GetCurrentClassLogger();

        public HandlerRemovedReactions(DiscordSocketClient client)
        {
            _client = client;
            embedUtils = new EmbedUtils();
        }

        public async Task Handle(Cacheable<IUserMessage, ulong> userMessage, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                var message = messageChannel.GetMessageAsync(userMessage.Id).Result;
                if (!reaction.User.Value.IsBot && message != null && message.Author.Id == _client.CurrentUser.Id)
                {
                    int embType = embedUtils.GetEmbedType(message);

                    switch (embType)
                    {
                        case 2:
                            await CountdownEmbedReactionHandler(message, messageChannel, reaction); break;
                        case 3:
                            //await RankingEmbedReactionHandler(message, messageChannel, reaction); 
                            break;
                        case 5:
                            await RLRankingEmbedReactionHandler(message, messageChannel, reaction); break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el Handle de RemovedReaction, exception: {e.Message}");
            }
        }

        private async Task CountdownEmbedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                using (var db = new EternalExodusEntities())
                {
                    var messageId = Convert.ToInt64(message.Id);
                    var userId = Convert.ToInt64(reaction.UserId);
                    var embed = db.CountdownEmbed.FirstOrDefault(x => x.EmbedId == messageId);
                    if (embed != null)
                    {
                        var notification = db.CountdownEmbedNotifications.FirstOrDefault(x => x.EmbedId == messageId && x.UserId == userId);
                        if (notification != null)
                        {
                            db.CountdownEmbedNotifications.Remove(notification);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el RemovedReactionHandler de eventos, exception: {e.Message}");
            }

            await Task.CompletedTask;
        }

        private async Task RankingEmbedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                switch (reaction.Emote.Name)
                {
                    case "RocketLeague":
                        await new RankingRocketLeagueForm(_client).DeleteUserRanking(reaction.User.Value);
                        break;
                    case "LeagueOfLegends":
                        await new RankingLeagueOfLegendsForm(_client).DeleteUserRanking(reaction.User.Value);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el RemovedReactionHandler de rankings, exception: {e.Message}");
            }
            
            await Task.CompletedTask;
        }

        private async Task RLRankingEmbedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                await new RankingRocketLeagueForm(_client).DeleteUserRanking(reaction.User.Value);
            }
            catch (Exception e)
            {
                log.Error($"Error en el RemovedReactionHandler de rankings, exception: {e.Message}");
            }

            await Task.CompletedTask;
        }
    }
}
