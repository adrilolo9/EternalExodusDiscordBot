﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.CommandDefinition.Forms;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands
{
    public class HandlerAddedReactions
    {
        private DiscordSocketClient _client;
        private EmbedUtils embedUtils;
        private Logger log = LogManager.GetCurrentClassLogger();

        public HandlerAddedReactions(DiscordSocketClient client)
        {
            _client = client;
            embedUtils = new EmbedUtils();
        }

        public async Task Handle(Cacheable<IUserMessage, ulong> userMessage, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                var message = messageChannel.GetMessageAsync(userMessage.Id).Result;
                if (!reaction.User.Value.IsBot && message != null && message.Author.Id == _client.CurrentUser.Id)
                {
                    int embType = embedUtils.GetEmbedType(message);

                    switch (embType)
                    {
                        case 2:
                            await CountdownEmbedReactionHandler(message, messageChannel, reaction); break;
                        case 3:
                            //await RankingEmbedReactionHandler(message, messageChannel, reaction); 
                            break;
                        case 4:
                            await SolicitudUserJoinedReactionHandler(message, messageChannel, reaction); break;
                        case 5:
                            await RLRankingEmbedReactionHandler(message, messageChannel, reaction); break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el Handle de AddedReaction, exception: {e.Message}");
            }
        }

        private async Task CountdownEmbedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                using (var db = new EternalExodusEntities())
                {
                    var messageId = Convert.ToInt64(message.Id);
                    var userId = Convert.ToInt64(reaction.UserId);
                    var countdownEmbed = db.CountdownEmbed.FirstOrDefault(x => x.EmbedId == messageId);
                    if (countdownEmbed != null)
                    {
                        db.CountdownEmbedNotifications.Add(new CountdownEmbedNotifications { EmbedId = messageId, NotificationPk = Guid.NewGuid().ToString().Replace("-", ""), UserId = userId, CreationTime = DateTime.Now });
                        db.SaveChanges();
                        await Task.CompletedTask;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el AddedReactionHandler de eventos, exception: {e.Message}");
            }
        }

        private async Task RankingEmbedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                switch (reaction.Emote.Name)
                {
                    case "RocketLeague":
                        new RankingRocketLeagueForm(_client).SendForm(reaction.User.Value).GetAwaiter();
                        break;
                    case "LeagueOfLegends":
                        new RankingLeagueOfLegendsForm(_client).SendForm(reaction.User.Value).GetAwaiter();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el AddedReactionHandler de rankings, exception: {e.Message}");
            }
            await Task.CompletedTask;
        }

        private async Task RLRankingEmbedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                new RankingRocketLeagueForm(_client).SendForm(reaction.User.Value).GetAwaiter();
            }
            catch (Exception e)
            {
                log.Error($"Error en el AddedReactionHandler de rankings, exception: {e.Message}");
            }
            await Task.CompletedTask;
        }

        private async Task SolicitudUserJoinedReactionHandler(IMessage message, ISocketMessageChannel messageChannel, SocketReaction reaction)
        {
            try
            {
                string status = message.Embeds.FirstOrDefault()?.Fields.FirstOrDefault(x => x.Name == "Estado de la solicitud").Value.Split('|')[1].Trim();

                if (status == "Pendiente")
                {
                    var countReactions = (message as IUserMessage).Reactions.FirstOrDefault(x => x.Key.Name == reaction.Emote.Name).Value.ReactionCount - 1;

                    if (countReactions >= 2)
                    {
                        var formService = new UserJoinedForm(_client);

                        if (reaction.Emote.Name == "Accepted")
                        {
                            await formService.AcceptUser(message, messageChannel);
                        }
                        else if (reaction.Emote.Name == "Denied")
                        {
                            await formService.DenyUser(message, messageChannel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Error en el AddedReactionHandler de solicitud user joined, exception: {e.Message}");
            }
            await Task.CompletedTask;
        }
    }
}
