﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public class RlShopItem
    {
        public string ItemName { get; set; }
        public string ItemDesc { get; set; }
        public string ItemRarity { get; set; }
        public string ItemType { get; set; }
        public string ImagePath { get; set; }
        public int Price { get; set; }
        public PaintsEnum ItemColor { get; set; }
        public string ItemCert { get; set; }
        public bool IsFeatured { get; set; }
        public int ExpiryTime { get; set; }
        public string ItemPromoImageUrl { get; set; }
    }
}
