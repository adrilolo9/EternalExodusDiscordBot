﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public enum ETXStatusEnum
    {
        Player = 1010,
        Captain = 1020,
        OnTrial = 1030,
        Alternate = 1040
    }
}
