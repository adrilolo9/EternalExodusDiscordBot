﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public class Item
    {
        public string itemName { get; set; }
        public byte[] itemPhoto { get; set; }
    }
}
