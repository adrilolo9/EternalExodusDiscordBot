﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public class TradeOffer
    {
        public string Username { get; set; }
        public PlatformEnum Platform { get; set; }
        public string TradeLink { get; set; }
        public string ProfileLink { get; set; }
        public string PlatformLink { get; set; }
        public string TradeDate { get; set; }
        public DateTime TradeExpiration { get; set; }
        public List<ItemOffer> ItemList { get; set; }
    }
}
