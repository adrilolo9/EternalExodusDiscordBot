﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public class EtxStructure
    {
        public List<EtxTeam> TeamsList { get; set; }
    }

    public class EtxTeam
    {
        public string TeamName { get; set; }
        public List<EtxPlayer> PlayersList { get; set; }
    }

    public class EtxPlayer
    {
        public string PlayerName { get; set; }
        public string PlayerTrackingId { get; set; }
        public bool IsSubstitute { get; set; }
    }
}
