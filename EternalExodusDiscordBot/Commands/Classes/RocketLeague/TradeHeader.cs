﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public class TradeHeader
    {
        public string Username { get; set; }
        public PlatformEnum Platform { get; set; }
        public string TradeLink { get; set; }
        public string ProfileLink { get; set; }
        public string TradeDate { get; set; }
    }
}
