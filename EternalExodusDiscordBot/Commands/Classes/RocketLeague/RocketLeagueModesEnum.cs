﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public enum RocketLeagueModesEnum
    {
        v1 = 1010,
        v2 = 1020,
        v3 = 1030,
        //v3Solo = 1040,
        Rumble = 1050,
        Dropshot = 1060,
        Hoops = 1070,
        Snowday = 1080
    }
}
