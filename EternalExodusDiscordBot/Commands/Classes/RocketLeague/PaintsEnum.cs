﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public enum PaintsEnum
    {
        None = 0,
        Lime = 1,
        TitaniumWhite = 2,
        Cobalt = 3,
        Crimson = 4,
        ForestGreen = 5,
        Grey = 6,
        Orange = 7,
        Pink = 8,
        Purple = 9,
        Saffron = 10,
        SkyBlue = 11,
        Black = 12,
        BurntSiena = 13
    }
}
