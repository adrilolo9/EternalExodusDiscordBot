﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public class ItemOffer
    {
        public Item Item { get; set; }
        public string Certificate { get; set; }
        public string Paint { get; set; }
        public int HaveWant { get; set; }
        public int Ammount { get; set; }
    }
}
