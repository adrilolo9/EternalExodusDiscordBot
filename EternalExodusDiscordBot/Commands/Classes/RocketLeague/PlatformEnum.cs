﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.RocketLeague
{
    public enum PlatformEnum
    {
        steam = 0,
        psn = 1,
        xbl = 2,
        nswitch = 3,
        unknown = 4,
    }
}
