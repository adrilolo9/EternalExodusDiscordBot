﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Events
{
    public class EtxUserJoined
    {
        public string Mention { get; set; }
        public string DiscordId { get; set; }
        public string InvitadoPor { get; set; }
        public string ReferenciasServidor { get; set; }
        public string BautizadoPublicador { get; set; }
        public string Congregacion { get; set; }
        public string PropositoDios { get; set; }
        public string ReinoDios { get; set; }
        public string Jesus144k { get; set; }
        public string JehovaCualidad { get; set; }
        public string ObraUrgente { get; set; }
        public string TematicaAtalaya { get; set; }
    }
}
