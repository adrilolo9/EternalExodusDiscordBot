﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends
{
    public enum LeagueOfLegendsModesEnum
    {
        Solo = 1010,
        Flex = 1020
    }
}
