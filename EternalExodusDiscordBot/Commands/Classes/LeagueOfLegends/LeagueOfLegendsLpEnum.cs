﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends
{
    public enum LeagueOfLegendsLpEnum
    {
        LpSolo = 1010,
        LpFlex = 1020
    }
}
