﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends
{
    public enum LeagueOfLegendsTiersEnum
    {
        TierSolo = 1010,
        TierFlex = 1020
    }
}
