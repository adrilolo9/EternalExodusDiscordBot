﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends
{
    public enum LeagueOfLegendsRanksEnum
    {
        RankSolo = 1010,
        RankFlex = 1020
    }
}
