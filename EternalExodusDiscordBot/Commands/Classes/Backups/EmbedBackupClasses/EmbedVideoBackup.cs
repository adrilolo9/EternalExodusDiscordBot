﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedVideoBackup
    {
        public string Url { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public EmbedVideoBackup(EmbedVideo video)
        {
            Url = video.Url;
            Height = video.Height;
            Width = video.Width;
        }
        [JsonConstructor]
        public EmbedVideoBackup()
        {

        }
    }
}
