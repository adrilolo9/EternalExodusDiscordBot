﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedThumbnailBackup
    {
        public string Url { get; set; }
        public string ProxyUrl { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public EmbedThumbnailBackup(EmbedThumbnail thumbnail)
        {
            Url = thumbnail.Url;
            ProxyUrl = thumbnail.ProxyUrl;
            Height = thumbnail.Height;
            Width = thumbnail.Width;
        }
        [JsonConstructor]
        public EmbedThumbnailBackup()
        {

        }
    }
}
