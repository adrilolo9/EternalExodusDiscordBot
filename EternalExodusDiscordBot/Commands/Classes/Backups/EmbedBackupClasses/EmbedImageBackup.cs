﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedImageBackup
    {
        public string Url { get; set; }
        public string ProxyUrl { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public EmbedImageBackup(Discord.EmbedImage image)
        {
            Url = image.Url;
            ProxyUrl = image.ProxyUrl;
            Height = image.Height;
            Width = image.Width;
        }
        [JsonConstructor]
        public EmbedImageBackup()
        {

        }
    }
}
