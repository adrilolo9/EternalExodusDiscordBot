﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedFieldBackup
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public bool Inline { get; set; }
        public EmbedFieldBackup(EmbedField field)
        {
            Name = field.Name;
            Value = field.Value;
            Inline = field.Inline;
        }
        [JsonConstructor]
        public EmbedFieldBackup()
        {

        }
    }
}
