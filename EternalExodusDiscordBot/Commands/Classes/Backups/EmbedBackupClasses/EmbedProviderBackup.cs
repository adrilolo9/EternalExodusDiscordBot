﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedProviderBackup
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public EmbedProviderBackup(EmbedProvider provider)
        {
            Name = provider.Name;
            Url = provider.Url;
        }
        [JsonConstructor]
        public EmbedProviderBackup()
        {

        }
    }
}
