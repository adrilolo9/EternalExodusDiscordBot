﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedAuthorBackup
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string IconUrl { get; set; }
        public string ProxyIconUrl { get; set; }
        public EmbedAuthorBackup(EmbedAuthor author)
        {
            Name = author.Name;
            Url = author.Url;
            IconUrl = author.IconUrl;
            ProxyIconUrl = author.ProxyIconUrl;
        }
        [JsonConstructor]
        public EmbedAuthorBackup()
        {

        }
    }
}
