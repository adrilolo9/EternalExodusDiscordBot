﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedColorBackup
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public EmbedColorBackup(Discord.Color color)
        {
            R = color.R;
            G = color.G;
            B = color.B;
        }
        [JsonConstructor]
        public EmbedColorBackup()
        {

        }
    }
}
