﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses
{
    [Serializable]
    public class EmbedFooterBackup
    {
        public string Text { get; set; }
        public string IconUrl { get; set; }
        public string ProxyUrl { get; set; }
        public EmbedFooterBackup(EmbedFooter footer)
        {
            Text = footer.Text;
            IconUrl = footer.IconUrl;
            ProxyUrl = footer.ProxyUrl;
        }
        [JsonConstructor]
        public EmbedFooterBackup()
        {

        }
    }
}
