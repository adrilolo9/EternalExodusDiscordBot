﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups
{
    [Serializable]
    public class MessageBackup
    {
        public string MessageContent { get; set; }
        public EmbedBackup Embed { get; set; }
        public AttachmentBackup Attachment { get; set; }
        public MessageBackup(IMessage message)
        {
            MessageContent = message.Content;
            Embed = message.Embeds.Any() ? new EmbedBackup(message.Embeds.First()) : null;
            Attachment = message.Attachments.Any() ? new AttachmentBackup(message.Attachments.First()) : null;
        }
        [JsonConstructor]
        public MessageBackup()
        {

        }
    }
}
