﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups
{
    [Serializable]
    public class AttachmentBackup
    {
        public string Url { get; set; }
        public AttachmentBackup(IAttachment attachment)
        {
            Url = attachment.Url;
        }
        [JsonConstructor]
        public AttachmentBackup()
        {

        }
    }
}
