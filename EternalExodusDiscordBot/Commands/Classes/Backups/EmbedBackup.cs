﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.Backups.EmbedBackupClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups
{
    [Serializable]
    public class EmbedBackup
    {
        public string Url { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public EmbedType Type { get; set; }

        public DateTimeOffset? Timestamp { get; set; }

        public EmbedColorBackup Color { get; set; }

        public EmbedImageBackup Image { get; set; }

        public EmbedVideoBackup Video { get; set; }

        public EmbedAuthorBackup Author { get; set; }

        public EmbedFooterBackup Footer { get; set; }

        public EmbedProviderBackup Provider { get; set; }

        public EmbedThumbnailBackup Thumbnail { get; set; }

        public List<EmbedFieldBackup> Fields { get; set; }
        
        public EmbedBackup(IEmbed embed)
        {
            Url = embed.Url;
            Title = embed.Title;
            Description = embed.Description;
            Type = embed.Type;
            Timestamp = embed.Timestamp;
            Color = embed.Color.HasValue ? new EmbedColorBackup(embed.Color.Value) : null;
            Image = embed.Image.HasValue ? new EmbedImageBackup(embed.Image.Value) : null;
            Video = embed.Video.HasValue ? new EmbedVideoBackup(embed.Video.Value) : null;
            Author = embed.Author.HasValue ? new EmbedAuthorBackup(embed.Author.Value) : null;
            Footer = embed.Footer.HasValue ? new EmbedFooterBackup(embed.Footer.Value) : null;
            Provider = embed.Provider.HasValue ? new EmbedProviderBackup(embed.Provider.Value) : null;
            Thumbnail = embed.Thumbnail.HasValue ? new EmbedThumbnailBackup(embed.Thumbnail.Value) : null;
            var fieldsList = new List<EmbedFieldBackup>();
            embed.Fields.ToList().ForEach(x => fieldsList.Add(new EmbedFieldBackup(x)));
            Fields = fieldsList;
        }
        [JsonConstructor]
        public EmbedBackup()
        {

        }
    }
}
