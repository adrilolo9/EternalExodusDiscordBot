﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Backups
{
    public class DiscordMessage
    {
        public string MessageContent { get; set; }
        public Embed Embed { get; set; }
        public string Attachment { get; set; }
    }
}
