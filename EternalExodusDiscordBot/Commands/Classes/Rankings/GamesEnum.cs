﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.Rankings
{
    public enum GamesEnum
    {
        MainRanking = 1000,
        RocketLeague = 1010,
        EtxAcademy = 1020,
        LeagueOfLegends = 1030
    }
}
