﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Classes.MagickNet
{
    public class MagickNetItemPaint
    {
        public MagickColor BorderColor { get; set; }
        public MagickColor FillColor { get; set; }
        public MagickColor TextColor { get; set; }
    }
}
