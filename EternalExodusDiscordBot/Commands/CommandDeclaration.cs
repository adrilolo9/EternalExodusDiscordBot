﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.LeagueOfLegends;
using EternalExodusDiscordBot.Commands.Classes.Rankings;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.CommandDefinition.Backups;
using EternalExodusDiscordBot.Commands.CommandDefinition.Embeds;
using EternalExodusDiscordBot.Commands.CommandDefinition.Forms;
using EternalExodusDiscordBot.Commands.CommandDefinition.JW;
using EternalExodusDiscordBot.Commands.CommandDefinition.Rankings;
using EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague;
using EternalExodusDiscordBot.Commands.CommandDefinition.ServerManagement;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using Newtonsoft.Json;
using NLog;
using RiotSharp.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands
{
    public class CommandDeclaration : ModuleBase<ICommandContext>
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        private EmbedUtils embedUtils = new EmbedUtils();
        private RocketLeagueUtils rlUtils = new RocketLeagueUtils();

        #region JW Commands

        [Command("texto")]
        [Summary("Returns daily text.")]
        public async Task GetDailyText()
        {
            await Context.Channel.SendMessageAsync("", embed: new DailyText().GetDailyText());
        }

        #endregion

        #region Rocket League Commands

        [Command("offer")]
        [Summary("Returns latest offer in https://rocket-league.com/trading.")]
        public async Task GetOffer()
        {
            await Context.Channel.SendMessageAsync(embed: new RocketTrades().GetTrade());
        }

        [Command("rlshop")]
        [Summary("Returns todays item shop.")]
        public async Task RocketLeagueItemShop()
        {
            if (Context.User.Id == 126445216071680000 || Context.User.Id == 302429301079539723)
            {
                var message = await Context.Channel.SendFileAsync(new RocketShop().GetItemShop());
            }
        }

        [Command("eshop")]
        [Summary("Returns todays esports shop.")]
        public async Task RocketLeagueEsportsShop()
        {
            if (Context.User.Id == 126445216071680000 || Context.User.Id == 302429301079539723)
            {
                var message = await Context.Channel.SendFileAsync(new RocketEsportsShop().GetRocketEShop());
            }
        }

        #endregion

        #region Embed Commands

        [Command("bienvenida")]
        [Summary("Returns todays esports shop.")]
        public async Task BienvenidaEmbed()
        {
            if (Context.User.Id == 126445216071680000 || Context.User.Id == 302429301079539723)
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.BienvenidaEmbed());
            }
        }

        [Command("evento")]
        [Summary("New event.")]
        public async Task GetCustomEmbed([Remainder]string time = null)
        {
            if (time == null)
            {
                var bme = await Context.Channel.SendMessageAsync(embed: embedUtils.BasicMessageEmbed());
                EmbedBuilder msgEmbedBuilder = bme.Embeds.First().ToEmbedBuilder();
                msgEmbedBuilder.WithTitle($"&eventotitulo {bme.Id} Titulo del embed");
                msgEmbedBuilder.WithDescription($"&eventodes {bme.Id} Descripcion del embed" + Environment.NewLine + $"&eventofoto {bme.Id} Url imagen" + Environment.NewLine + $"&eventominiatura {bme.Id} Url imagen (miniatura)");
                msgEmbedBuilder.WithFooter($"Message ID: {bme.Id} | Embed type: 1");

                await bme.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());
                await Context.Message.DeleteAsync();

                using (var db = new EternalExodusEntities())
                {
                    var msgId = Convert.ToInt64(bme.Id);
                    var userId = Convert.ToInt64(Context.User.Id);
                    var channelId = Convert.ToInt64(Context.Channel.Id);
                    db.BasicEmbed.Add(new BasicEmbed { CreationTime = DateTime.Now, EmbedId = msgId, UserId = userId, ChannelId = channelId });
                    db.SaveChanges();
                }

                return;
            }

            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                return;
            }

            var cem = new CustomEmbedMessage();
            var message = await Context.Channel.SendMessageAsync(embed: embedUtils.BasicMessageEmbed());
            await cem.NewCustomEmbed(message, time, Context);

            using (var db = new EternalExodusEntities())
            {
                var msgId = Convert.ToInt64(message.Id);
                var cemb = db.CountdownEmbed.FirstOrDefault(x => x.EmbedId == msgId);
                cem.CountdownEmbed(cemb, Context.Client as DiscordSocketClient).GetAwaiter();
            }
        }

        [Command("eventotitulo")]
        [Summary("Edit event title.")]
        public async Task EditTitleCustomEmbed(ulong msgId, [Remainder]string title)
        {
            IUserMessage message = (Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage;

            if (embedUtils.GetEmbedType(message) == 2)
            {
                if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
                {
                    await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                    return;
                }

                if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
                {
                    await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                    return;
                }
            }

            EmbedBuilder msgEmbedBuilder = message.Embeds.First().ToEmbedBuilder();
            msgEmbedBuilder.Title = title;

            await message.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());

            await Context.Message.DeleteAsync();
        }

        [Command("eventodes")]
        [Summary("Edit event description.")]
        public async Task EditDescCustomEmbed(ulong msgId, [Remainder]string desc)
        {
            IUserMessage message = (Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage;

            if (embedUtils.GetEmbedType(message) == 1)
            {
                EmbedBuilder msgEmbBuilder = message.Embeds.First().ToEmbedBuilder();
                msgEmbBuilder.Description = desc;
                await message.ModifyAsync(x => x.Embed = msgEmbBuilder.Build());
                await Context.Message.DeleteAsync();
                return;
            }

            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                return;
            }


            EmbedBuilder msgEmbedBuilder = message.Embeds.First().ToEmbedBuilder();
            msgEmbedBuilder.Description = desc + Environment.NewLine + Environment.NewLine + "🔔 **Toca la campana para ser notificado cuando el evento comience.**";

            await message.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());

            await Context.Message.DeleteAsync();
        }

        [Command("eventourl")]
        [Summary("Edit event description.")]
        public async Task EditUrlCustomEmbed(ulong msgId, string url)
        {
            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                return;
            }

            IUserMessage message = (Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage;
            EmbedBuilder msgEmbedBuilder = message.Embeds.First().ToEmbedBuilder();
            msgEmbedBuilder.Url = url;

            await message.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());

            await Context.Message.DeleteAsync();
        }

        [Command("eventominiatura")]
        [Summary("Edit event thumbnail.")]
        public async Task EditThumbnailCustomEmbed(ulong msgId, string url)
        {
            IUserMessage message = (Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage;

            if (embedUtils.GetEmbedType(message) == 2)
            {
                if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
                {
                    await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                    return;
                }

                if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
                {
                    await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                    return;
                }
            }

            EmbedBuilder msgEmbedBuilder = message.Embeds.First().ToEmbedBuilder();
            msgEmbedBuilder.ThumbnailUrl = url;

            await message.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());

            await Context.Message.DeleteAsync();
        }

        [Command("eventofoto")]
        [Summary("Edit event thumbnail.")]
        public async Task EditImageCustomEmbed(ulong msgId, string url)
        {
            IUserMessage message = (Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage;

            if (embedUtils.GetEmbedType(message) == 2)
            {
                if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
                {
                    await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                    return;
                }

                if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
                {
                    await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                    return;
                }
            }

            EmbedBuilder msgEmbedBuilder = message.Embeds.First().ToEmbedBuilder();
            msgEmbedBuilder.ImageUrl = url;

            await message.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());

            await Context.Message.DeleteAsync();
        }

        [Command("eventocolor")]
        [Summary("Edit event color.")]
        public async Task EditColorCustomEmbed(ulong msgId, string hex)
        {
            IUserMessage message = (Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage;

            if (embedUtils.GetEmbedType(message) == 2)
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar para eventos de type 2."));
                return;
            }

            EmbedBuilder msgEmbedBuilder = message.Embeds.First().ToEmbedBuilder();
            var rgb = ColorUtils.HexadecimalToRGB(hex);
            msgEmbedBuilder.Color = new Color(rgb.r, rgb.g, rgb.b);

            await message.ModifyAsync(x => x.Embed = msgEmbedBuilder.Build());

            await Context.Message.DeleteAsync();
        }

        [Command("borrarevento")]
        [Summary("Cancel event.")]
        public async Task CancelCustomEmbed(ulong msgId)
        {
            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser) || !(Context.User as SocketGuildUser).Roles.Any(x => x.Id == 705830145809842187))) //Rol encargados ETX
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            if (Context.Channel.Id != 722841838234632222 && Context.Channel.Id != 689170543316172900)
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("Este comando no se puede ejecutar en este canal."));
                return;
            }

            if (EmbedUtils.cancellationTokens.ContainsKey(msgId))
            {
                var cancelToken = EmbedUtils.cancellationTokens.First(x => x.Key == msgId);
                cancelToken.Value.Cancel();
                EmbedUtils.cancellationTokens.Remove(msgId);
                await ((Context.Channel as SocketTextChannel).GetMessageAsync(msgId).Result as IUserMessage).DeleteAsync();
                await Context.Message.DeleteAsync();

                using (var db = new EternalExodusEntities())
                {
                    var messageId = Convert.ToInt64(msgId);
                    var embed = db.CountdownEmbed.FirstOrDefault(x => x.EmbedId == messageId);
                    if (embed != null)
                    {
                        var notifications = db.CountdownEmbedNotifications.Where(x => x.EmbedId == messageId).ToList();
                        if (notifications.Count > 0)
                        {
                            db.CountdownEmbedNotifications.RemoveRange(notifications);
                        }

                        db.CountdownEmbed.Remove(embed);
                        db.SaveChanges();

                        await Task.CompletedTask;
                    }
                }
            }
            else
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.ErrorEmbed($"KeyValuePair for the message id {msgId} not found, can't cancel the embed."));
                return;
            }
        }

        #endregion

        #region Ranking Embed Commands

        /*[Command("mainranking")]
        [Summary("Sets a channel as the subscription channel for rankings.")]
        public async Task GetMainRankingEmb()
        {
            await Context.Message.DeleteAsync();

            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser)))
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            using (var db = new EternalExodusEntities())
            {
                var gameId = GamesEnum.MainRanking.GetHashCode();
                var embedType = EmbedEnum.Subscription.GetHashCode();
                var embed = db.RankingEmbeds.FirstOrDefault(x => x.GameId == gameId && x.EmbedType == embedType);
                if (embed != null)
                {
                    try
                    {
                        var channel = Context.Guild.GetChannelAsync((ulong)embed.ChannelId).Result as ITextChannel;
                        var message = channel.GetMessageAsync((ulong)embed.EmbedId).Result;
                        await channel.DeleteMessageAsync(message);
                    }
                    catch (Exception e)
                    {
                        log.Error($"Error borrando embed: {e.Message}");
                    }

                    db.RankingEmbeds.Remove(embed);
                    db.SaveChanges();
                }
            }

            MainRanking mr = new MainRanking();
            var channelId = Convert.ToInt64(Context.Channel.Id);

            var newEmb = await Context.Channel.SendMessageAsync(embed: mr.UpdateMainRanking(Context.Client as DiscordSocketClient));
            await newEmb.AddReactionsAsync(new IEmote[] { EmoteUtils.GetEmoteByName("RocketLeague"), EmoteUtils.GetEmoteByName("LeagueOfLegends") });
            var messageId = Convert.ToInt64(newEmb.Id);

            using (var db = new EternalExodusEntities())
            {
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = messageId, GameId = GamesEnum.MainRanking.GetHashCode(), EmbedType = EmbedEnum.Subscription.GetHashCode() });
                db.SaveChanges();
            }
        }*/

        [Command("rlinscription")]
        [Summary("Returns ranking inscription embed for RL.")]
        public async Task RocketLeagueInscriptionEmbed()
        {
            if (Context.User.Id == 126445216071680000 || Context.User.Id == 302429301079539723)
            {
                var message = await Context.Channel.SendMessageAsync(embed:rlUtils.RocketInscriptionEmbed());
            }
        }

        [Command("rankingrl")]
        [Summary("Sets a channel to show the Rocket League rankings periodically.")]
        public async Task GetRankingEmbRl()
        {
            await Context.Message.DeleteAsync();

            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser))) //Rol encargados ETX
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            using (var db = new EternalExodusEntities())
            {
                var gameId = GamesEnum.RocketLeague.GetHashCode();
                var embedType = EmbedEnum.Ranking.GetHashCode();
                var embeds = db.RankingEmbeds.Where(x => x.GameId == gameId && x.EmbedType == embedType).ToList();
                foreach (var embed in embeds)
                {
                    try
                    {
                        var channel = Context.Guild.GetChannelAsync((ulong)embed.ChannelId).Result as ITextChannel;
                        var message = channel.GetMessageAsync((ulong)embed.EmbedId).Result;
                        await channel.DeleteMessageAsync(message);
                    }
                    catch (Exception e)
                    {
                        log.Error($"Error borrando embed: {e.Message}");
                    }

                    db.RankingEmbeds.Remove(embed);
                }
                db.SaveChanges();
            }

            var channelId = Convert.ToInt64(Context.Channel.Id);

            //Send embed for Snowday
            var rSnowday = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - SNOWDAY**")).Result;
            var rSnowdayId = Convert.ToInt64(rSnowday.Id);

            //Send embed for Hoops
            var rHoops = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - HOOPS**")).Result;
            var rHoopsId = Convert.ToInt64(rHoops.Id);

            //Send embed for Dropshot
            var rDropshot = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - DROPSHOT**")).Result;
            var rDropshotId = Convert.ToInt64(rDropshot.Id);

            //Send embed for Rumble
            var rRumble = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - RUMBLE**")).Result;
            var rRumbleId = Convert.ToInt64(rRumble.Id);

            //Send embed for 3v3 ranking
            var r3v3 = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - 3VS3**")).Result;
            var r3v3Id = Convert.ToInt64(r3v3.Id);

            //Send embed for 2v2 ranking
            var r2v2 = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - 2VS2**")).Result;
            var r2v2Id = Convert.ToInt64(r2v2.Id);

            //Send embed for 1v1 ranking
            var r1v1 = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - 1VS1**")).Result;
            var r1v1Id = Convert.ToInt64(r1v1.Id);

            using (var db = new EternalExodusEntities())
            {
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = r1v1Id, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.v1.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = r2v2Id, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.v2.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = r3v3Id, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.v3.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = rRumbleId, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.Rumble.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = rDropshotId, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.Dropshot.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = rHoopsId, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.Hoops.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = rSnowdayId, GameId = GamesEnum.RocketLeague.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = RocketLeagueModesEnum.Snowday.GetHashCode(), PageNum = 1 });
                db.SaveChanges();
            }
        }

        [Command("etxranking")]
        [Summary("Sets a channel as the ranking channel for etx academy.")]
        public async Task GetEtxRankingEmb()
        {
            await Context.Message.DeleteAsync();

            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser)))
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            using (var db = new EternalExodusEntities())
            {
                var gameId = GamesEnum.EtxAcademy.GetHashCode();
                var embedType = EmbedEnum.Ranking.GetHashCode();
                var embed = db.RankingEmbeds.FirstOrDefault(x => x.GameId == gameId && x.EmbedType == embedType);
                if (embed != null)
                {
                    try
                    {
                        var channel = Context.Guild.GetChannelAsync((ulong)embed.ChannelId).Result as ITextChannel;
                        var message = channel.GetMessageAsync((ulong)embed.EmbedId).Result;
                        await channel.DeleteMessageAsync(message);
                    }
                    catch (Exception e)
                    {
                        log.Error($"Error borrando embed: {e.Message}");
                    }

                    db.RankingEmbeds.Remove(embed);
                    db.SaveChanges();
                }

                var channelId = Convert.ToInt64(Context.Channel.Id);
                var etxPlayers = db.RankingsRocketLeague.Where(x => x.ETXAcademy != 0).ToList();
                var newEmb = await Context.Channel.SendMessageAsync(embed: new RocketLeagueUtils().EtxAcademyRankingsEmbed(etxPlayers));
                var messageId = Convert.ToInt64(newEmb.Id);

                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = messageId, GameId = gameId, EmbedType = embedType, PageNum = 1 });
                db.SaveChanges();
            }
        }

        [Command("lolranking")]
        [Summary("Sets a channel as the ranking channel for League Of Legends.")]
        public async Task GetLOLRankingEmb()
        {
            await Context.Message.DeleteAsync();

            if (Context.User.Id != 126445216071680000 && (!(Context.User is SocketGuildUser))) //Rol encargados ETX
            {
                await Context.Channel.SendMessageAsync(embed: embedUtils.PermissionErrorEmbed("No tienes permiso para ejecutar este comando / este comando no se puede ejecutar en DM."));
                return;
            }

            using (var db = new EternalExodusEntities())
            {
                var gameId = GamesEnum.LeagueOfLegends.GetHashCode();
                var embedType = EmbedEnum.Ranking.GetHashCode();
                var embeds = db.RankingEmbeds.Where(x => x.GameId == gameId && x.EmbedType == embedType).ToList();
                foreach (var embed in embeds)
                {
                    try
                    {
                        var channel = Context.Guild.GetChannelAsync((ulong)embed.ChannelId).Result as ITextChannel;
                        var message = channel.GetMessageAsync((ulong)embed.EmbedId).Result;
                        await channel.DeleteMessageAsync(message);
                    }
                    catch (Exception e)
                    {
                        log.Error($"Error borrando embed: {e.Message}");
                    }

                    db.RankingEmbeds.Remove(embed);
                }
                db.SaveChanges();
            }

            var channelId = Convert.ToInt64(Context.Channel.Id);

            //Send embed for Solo
            var rSolo = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - SOLO/DUO**")).Result;
            var rSoloId = Convert.ToInt64(rSolo.Id);

            //Send embed for Flex
            var rFlex = Context.Channel.SendMessageAsync(embed: rlUtils.RankingEmbed("**RANKING - FLEX**")).Result;
            var rFlexId = Convert.ToInt64(rFlex.Id);

            using (var db = new EternalExodusEntities())
            {
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = rFlexId, GameId = GamesEnum.LeagueOfLegends.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = LeagueOfLegendsModesEnum.Flex.GetHashCode(), PageNum = 1 });
                db.RankingEmbeds.Add(new RankingEmbeds() { ChannelId = channelId, EmbedId = rSoloId, GameId = GamesEnum.LeagueOfLegends.GetHashCode(), EmbedType = EmbedEnum.Ranking.GetHashCode(), GameMode = LeagueOfLegendsModesEnum.Solo.GetHashCode(), PageNum = 1 });
                db.SaveChanges();
            }
        }

        #endregion

        #region Backups

        [Command("backup")]
        [Summary("Backup of current channel messages.")]
        public async Task BackupChannelMessages()
        {
            BackupService bs = new BackupService();

            await bs.SetChannelBackup(Context);
        }

        [Command("restore")]
        [Summary("Restore current channel messages.")]
        public async Task RestoreChannelMessages([Remainder]string channelName = null)
        {
            BackupService bs = new BackupService();

            await bs.RestoreChannelMessages(Context, channelName);
        }

        #endregion
    }
}
