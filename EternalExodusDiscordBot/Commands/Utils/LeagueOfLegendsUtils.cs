﻿using EternalExodusDiscordBot.Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public static class LeagueOfLegendsUtils
    {
        public static string GetTierEmoji(object tier)
        {
            if (tier != null)
            {
                switch (tier.ToString())
                {
                    case "IRON": return "LolHierro";
                    case "BRONZE": return "LolBronce";
                    case "SILVER": return "LolPlata";
                    case "GOLD": return "LolOro";
                    case "PLATINUM": return "LolPlatino";
                    case "DIAMOND": return "LolDiamante";
                    case "MASTER": return "LolMaestro";
                    case "GRANDMASTER": return "LolGranMaestro";
                    case "CHALLENGER": return "LolChallenger";
                    default: return "LolUnranked";
                }
            }
            else
            {
                return "LolUnranked";
            }
        }

        public static int GetRankPoints(object tier, object rank, object lp)
        {
            int tierPoints;
            int rankPoints;

            if (tier != null)
            {
                switch (tier.ToString())
                {
                    case "IRON": tierPoints = 1000; break;
                    case "BRONZE": tierPoints = 2000; break;
                    case "SILVER": tierPoints = 3000; break;
                    case "GOLD": tierPoints = 4000; break;
                    case "PLATINUM": tierPoints = 5000; break;
                    case "DIAMOND": tierPoints = 6000; break;
                    case "MASTER": tierPoints = 7000; break;
                    case "GRANDMASTER": tierPoints = 8000; break;
                    case "CHALLENGER": tierPoints = 9000; break;
                    default: tierPoints = 0; break;
                }
            }
            else
            {
                tierPoints = 0;
            }

            if (rank != null)
            {
                switch (rank.ToString())
                {
                    case "I": rankPoints = 400; break;
                    case "II": rankPoints = 300; break;
                    case "III": rankPoints = 200; break;
                    case "IV": rankPoints = 100; break;
                    default: rankPoints = 0; break;
                }
            }
            else
            {
                rankPoints = 0;
            }

            if (lp == null)
            {
                lp = 0;
            }

            return tierPoints + rankPoints + (int)lp;
        }
    }
}
