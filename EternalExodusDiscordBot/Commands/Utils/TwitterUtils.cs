﻿using EternalExodusDiscordBot.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Core.Models;
using Tweetinvi.Parameters;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public class TwitterUtils
    {
        private TwitterClient client;
        
        public TwitterUtils()
        {
            client = new TwitterClient(BotSettings.Default.TwitterApiKey, BotSettings.Default.TwitterApiSecretKey, BotSettings.Default.TwitterBearerToken);
        }

        public async Task GetRlEsportsShop()
        {
            var user = await client.Users.GetUserAsync(1121853429834502144);

            var search = new SearchTweetsParameters($"from:{user.ScreenName} Esports shop for {DateTime.Now.ToString("%M")}/{DateTime.Now.ToString("%d")}");

            var tweets = await client.Search.SearchTweetsAsync(search);

            if (tweets.Count() > 0)
            {
                var eshop = tweets[0];
                var shopImageUrl = eshop.Media.FirstOrDefault()?.MediaURLHttps;
                if (!string.IsNullOrEmpty(shopImageUrl))
                {
                    WebClient client = new WebClient();
                    var fileName = $"Resources/RocketShop/eshop.jpg";
                    client.DownloadFile(new Uri(shopImageUrl), $@"{fileName}");
                }
            }

            await Task.CompletedTask;
        }
    }
}
