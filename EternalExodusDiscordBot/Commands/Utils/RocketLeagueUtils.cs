﻿using Discord;
using EternalExodusDiscordBot.Commands.Classes.MagickNet;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public class RocketLeagueUtils
    {
        public static string GetRankEmoji(int rank, RocketLeagueModesEnum mode)
        {
            if (mode == RocketLeagueModesEnum.v1)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 145:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n >= 145 && n < 202:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 202 && n < 265:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 265 && n < 321:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 321 && n < 384:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 384 && n < 455:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 455 && n < 513:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 513 && n < 573:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 573 && n < 635:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 635 && n < 687:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 687 && n < 747:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 747 && n < 807:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 807 && n < 867:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 867 && n < 925:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 925 && n < 987:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 987 && n < 1046:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1046 && n < 1105:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1105 && n < 1175:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1175 && n < 1226:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1226 && n < 1287:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1287 && n < 1345:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1345:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }
            else if (mode == RocketLeagueModesEnum.v2)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 184:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n >= 184 && n < 246:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 246 && n < 308:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 308 && n < 370:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 370 && n < 430:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 430 && n < 491:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 491 && n < 552:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 552 && n < 614:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 614 && n < 694:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 694 && n < 773:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 773 && n < 853:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 853 && n < 933:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 933 && n < 1014:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 1014 && n < 1115:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 1115 && n < 1214:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 1214 && n < 1313:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1313 && n < 1414:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1414 && n < 1515:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1515 && n < 1635:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1635 && n < 1752:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1752 && n < 1863:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1863:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }
            else if (mode == RocketLeagueModesEnum.v3)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 181:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n >= 181 && n < 248:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 248 && n < 307:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 307 && n < 370:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 370 && n < 432:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 432 && n < 494:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 494 && n < 549:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 549 && n < 615:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 615 && n < 695:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 695 && n < 775:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 775 && n < 854:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 854 && n < 935:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 935 && n < 1034:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 1034 && n < 1135:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 1135 && n < 1233:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 1233 && n < 1334:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1334 && n < 1435:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1435 && n < 1555:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1555 && n < 1672:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1672 && n < 1794:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1794 && n < 1910:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1910:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }
            else if (mode == RocketLeagueModesEnum.Rumble)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 152:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n >= 152 && n < 211:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 211 && n < 267:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 267 && n < 323:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 323 && n < 388:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 388 && n < 447:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 447 && n < 513:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 513 && n < 575:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 575 && n < 635:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 635 && n < 695:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 695 && n < 753:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 753 && n < 812:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 812 && n < 870:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 870 && n < 927:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 927 && n < 992:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 992 && n < 1047:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1047 && n < 1106:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1106 && n < 1168:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1168 && n < 1228:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1228 && n < 1287:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1287 && n < 1343:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1343:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }
            else if (mode == RocketLeagueModesEnum.Dropshot)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 120:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n > 120 && n < 180:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 180 && n < 226:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 226 && n < 283:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 283 && n < 341:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 341 && n < 402:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 402 && n < 471:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 471 && n < 524:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 524 && n < 595:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 595 && n < 655:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 655 && n < 715:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 715 && n < 775:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 775 && n < 827:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 827 && n < 890:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 890 && n < 955:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 955 && n < 1005:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1005 && n < 1065:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1065 && n < 1126:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1126 && n < 1181:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1181 && n < 1241:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1241 && n < 1315:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1315:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }
            else if (mode == RocketLeagueModesEnum.Hoops)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 152:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n >= 152 && n < 201:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 201 && n < 271:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 271 && n < 325:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 325 && n < 384:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 384 && n < 449:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 449 && n < 508:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 508 && n < 563:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 563 && n < 635:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 635 && n < 695:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 695 && n < 753:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 753 && n < 806:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 806 && n < 865:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 865 && n < 925:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 925 && n < 990:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 990 && n < 1041:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1041 && n < 1103:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1103 && n < 1163:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1163 && n < 1227:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1227 && n < 1284:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1284 && n < 1284:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1355:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }
            else if (mode == RocketLeagueModesEnum.Snowday)
            {
                switch (rank)
                {
                    case 0:
                        return EmoteUtils.GetEmoteStringByName("Unranked");
                    case int n when n > 0 && n < 154:
                        return EmoteUtils.GetEmoteStringByName("Bronce1");
                    case int n when n >= 154 && n < 217:
                        return EmoteUtils.GetEmoteStringByName("Bronce2");
                    case int n when n >= 217 && n < 277:
                        return EmoteUtils.GetEmoteStringByName("Bronce3");
                    case int n when n >= 277 && n < 321:
                        return EmoteUtils.GetEmoteStringByName("Plata1");
                    case int n when n >= 321 && n < 383:
                        return EmoteUtils.GetEmoteStringByName("Plata2");
                    case int n when n >= 383 && n < 451:
                        return EmoteUtils.GetEmoteStringByName("Plata3");
                    case int n when n >= 451 && n < 513:
                        return EmoteUtils.GetEmoteStringByName("Oro1");
                    case int n when n >= 513 && n < 563:
                        return EmoteUtils.GetEmoteStringByName("Oro2");
                    case int n when n >= 563 && n < 635:
                        return EmoteUtils.GetEmoteStringByName("Oro3");
                    case int n when n >= 635 && n < 684:
                        return EmoteUtils.GetEmoteStringByName("Platino1");
                    case int n when n >= 684 && n < 755:
                        return EmoteUtils.GetEmoteStringByName("Platino2");
                    case int n when n >= 755 && n < 815:
                        return EmoteUtils.GetEmoteStringByName("Platino3");
                    case int n when n >= 815 && n < 875:
                        return EmoteUtils.GetEmoteStringByName("Diamante1");
                    case int n when n >= 875 && n < 927:
                        return EmoteUtils.GetEmoteStringByName("Diamante2");
                    case int n when n >= 927 && n < 985:
                        return EmoteUtils.GetEmoteStringByName("Diamante3");
                    case int n when n >= 985 && n < 1045:
                        return EmoteUtils.GetEmoteStringByName("Campeon1");
                    case int n when n >= 1045 && n < 1108:
                        return EmoteUtils.GetEmoteStringByName("Campeon2");
                    case int n when n >= 1108 && n < 1168:
                        return EmoteUtils.GetEmoteStringByName("Campeon3");
                    case int n when n >= 1168 && n < 1233:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon1");
                    case int n when n >= 1233 && n < 1298:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon2");
                    case int n when n >= 1298 && n < 1356:
                        return EmoteUtils.GetEmoteStringByName("GranCampeon3");
                    case int n when n >= 1356:
                        return EmoteUtils.GetEmoteStringByName("SupersonicLegend");
                    default:
                        return "";
                }
            }

            return "";
        }

        public static string GetModeName(RocketLeagueModesEnum mode)
        {
            switch (mode)
            {
                case RocketLeagueModesEnum.v1: return "1VS1";
                case RocketLeagueModesEnum.v2: return "2VS2";
                case RocketLeagueModesEnum.v3: return "3VS3";
                case RocketLeagueModesEnum.Rumble: return "RUMBLE";
                case RocketLeagueModesEnum.Snowday: return "SNOWDAY";
                case RocketLeagueModesEnum.Hoops: return "HOOPS";
                case RocketLeagueModesEnum.Dropshot: return "DROPSHOT";
                default: return "";
            }
        }

        private static string GetChangeSign(int change)
        {
            if (change >= 0)
            {
                return "+";
            }
            else
            {
                return "";
            }
        }

        private static string GetETXChangeEmoji(int change)
        {
            switch (change)
            {
                case int n when n > 0:
                    return EmoteUtils.GetEmoteStringByName("UpVote");
                case int n when n == 0:
                    return EmoteUtils.GetEmoteStringByName("Offline");
                case int n when n < 0:
                    return EmoteUtils.GetEmoteStringByName("DownVote");
                default:
                    return "";
            }
        }

        public static string GetETXStatus(ETXStatusEnum etxStatus)
        {
            switch (etxStatus)
            {
                case ETXStatusEnum.Player:
                    return "";
                case ETXStatusEnum.Captain:
                    return $" {EmoteUtils.GetEmoteStringByName("Capitan")}";
                case ETXStatusEnum.OnTrial:
                    return $" {EmoteUtils.GetEmoteStringByName("Pruebas")}";
                case ETXStatusEnum.Alternate:
                    return $" {EmoteUtils.GetEmoteStringByName("Substituto")}";
                default:
                    return "";
            }
        }

        public static (string Name, string Desc) GetItemNameDesc(string fullName, string itemType, string itemEdition = "")
        {
            var splittedValues = fullName.Split(':');

            if (splittedValues.Length > 1)
            {
                itemEdition = string.IsNullOrEmpty(itemEdition) ? "" : " " + itemEdition;
                switch (itemType)
                {
                    case "Decal":
                        return (splittedValues[1].Trim(), splittedValues[0].Trim() + itemEdition);
                    case "Wheels":
                        return (splittedValues[0].Trim(), splittedValues[1].Trim() + itemEdition);
                    default:
                        return (splittedValues[0].Trim(), splittedValues[1].Trim() + itemEdition);
                }
            }
            else
            {
                return (fullName, "" + itemEdition);
            }
        }

        public static string GetItemRarity(string rarity)
        {
            switch (rarity)
            {
                case "uncommon":
                    return "Uncommon";
                case "rare":
                    return "Rare";
                case "veryrare":
                    return "Very Rare";
                case "import":
                    return "Import";
                case "exotic":
                    return "Exotic";
                case "blackmarket":
                    return "Black Market";
                case "limited":
                    return "Limited";
                default:
                    return "";
            }
        }

        public static MagickNetItemPaint GetItemPaint(PaintsEnum color)
        {
            switch (color)
            {
                case PaintsEnum.BurntSiena:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(76, 17, 0, 255),
                        FillColor = MagickColor.FromRgba(76, 17, 0, 170),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Lime:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(127, 255, 0, 255),
                        FillColor = MagickColor.FromRgba(127, 255, 0, 150),
                        TextColor = MagickColor.FromRgba(0, 0, 0, 255)
                    };
                case PaintsEnum.TitaniumWhite:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(255, 255, 255, 255),
                        FillColor = MagickColor.FromRgba(255, 255, 255, 170),
                        TextColor = MagickColor.FromRgba(0, 0, 0, 255)
                    };
                case PaintsEnum.Cobalt:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(63, 81, 181, 255),
                        FillColor = MagickColor.FromRgba(63, 81, 181, 190),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Crimson:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(213, 0, 0, 255),
                        FillColor = MagickColor.FromRgba(213, 0, 0, 150),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.ForestGreen:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(76, 175, 80, 255),
                        FillColor = MagickColor.FromRgba(76, 150, 80, 190),
                        TextColor = MagickColor.FromRgba(0, 0, 0, 255)
                    };
                case PaintsEnum.Grey:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(119, 119, 119, 255),
                        FillColor = MagickColor.FromRgba(119, 119, 119, 170),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Orange:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(244, 180, 0, 255),
                        FillColor = MagickColor.FromRgba(244, 180, 0, 170),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Pink:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(255, 64, 129, 255),
                        FillColor = MagickColor.FromRgba(255, 64, 129, 120),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Purple:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(156, 39, 176, 255),
                        FillColor = MagickColor.FromRgba(156, 39, 176, 180),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Saffron:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(255, 235, 59, 255),
                        FillColor = MagickColor.FromRgba(255, 235, 59, 150),
                        TextColor = MagickColor.FromRgba(0, 0, 0, 255)
                    };
                case PaintsEnum.SkyBlue:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(3, 169, 244, 255),
                        FillColor = MagickColor.FromRgba(3, 169, 244, 150),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.Black:
                    return new MagickNetItemPaint()
                    {
                        BorderColor = MagickColor.FromRgba(0, 0, 0, 255),
                        FillColor = MagickColor.FromRgba(50, 50, 50, 190),
                        TextColor = MagickColor.FromRgba(255, 255, 255, 255)
                    };
                case PaintsEnum.None:
                    return null;
                default:
                    return null;
            }
        }

        public Embed EtxAcademyRankingsEmbed(List<RankingsRocketLeague> rrlList)
        {
            var etxFieldDesc = $"";
            var etxaFieldDesc = $"";
            var etxbFieldDesc = $"";

            var etxCount = 0;
            var etxaCount = 0;
            var etxbCount = 0;

            var etxNewSumRanks = 0;
            var etxaNewSumRanks = 0;
            var etxbNewSumRanks = 0;

            var etxOldSumRanks = 0;
            var etxaOldSumRanks = 0;
            var etxbOldSumRanks = 0;

            var etxPlayers = rrlList.Where(x => x.ETXAcademy == 1 && x.ETXStauts != 1040).OrderByDescending(x => x.v3).ToList();
            var etxaPlayers = rrlList.Where(x => x.ETXAcademy == 2 && x.ETXStauts != 1040).OrderByDescending(x => x.v3).ToList();
            var etxbPlayers = rrlList.Where(x => x.ETXAcademy == 3 && x.ETXStauts != 1040).OrderByDescending(x => x.v3).ToList();

            var etxAlternates = rrlList.Where(x => x.ETXAcademy == 1 && x.ETXStauts == 1040).OrderByDescending(x => x.v3).ToList();
            var etxaAlternates = rrlList.Where(x => x.ETXAcademy == 2 && x.ETXStauts == 1040).OrderByDescending(x => x.v3).ToList();
            var etxbAlternates = rrlList.Where(x => x.ETXAcademy == 3 && x.ETXStauts == 1040).OrderByDescending(x => x.v3).ToList();

            foreach (var etx in etxPlayers)
            {
                etxFieldDesc += $"{GetETXChangeEmoji(etx.ETXPosition.Value - (etxPlayers.IndexOf(etx) + 1))} - {RocketLeagueUtils.GetRankEmoji(etx.v3.Value, RocketLeagueModesEnum.v3)} {etx.Username} ({etx.v3}) {GetChangeSign(etx.v3.Value - etx.v3Daily.Value)}{etx.v3.Value - etx.v3Daily.Value}{GetETXStatus((ETXStatusEnum)etx.ETXStauts)}" + Environment.NewLine;
                etxCount++;
                etxNewSumRanks += etx.v3.Value;
                etxOldSumRanks += etx.v3Daily.Value;
            }

            foreach (var etxa in etxaPlayers)
            {
                etxaFieldDesc += $"{GetETXChangeEmoji(etxa.ETXPosition.Value - (etxaPlayers.IndexOf(etxa) + 1))} - {RocketLeagueUtils.GetRankEmoji(etxa.v3.Value, RocketLeagueModesEnum.v3)} {etxa.Username} ({etxa.v3}) {GetChangeSign(etxa.v3.Value - etxa.v3Daily.Value)}{etxa.v3.Value - etxa.v3Daily.Value}{GetETXStatus((ETXStatusEnum)etxa.ETXStauts)}" + Environment.NewLine;
                etxaCount++;
                etxaNewSumRanks += etxa.v3.Value;
                etxaOldSumRanks += etxa.v3Daily.Value;
            }

            foreach (var etxb in etxbPlayers)
            {
                etxbFieldDesc += $"{GetETXChangeEmoji(etxb.ETXPosition.Value - (etxbPlayers.IndexOf(etxb) + 1))} - {RocketLeagueUtils.GetRankEmoji(etxb.v3.Value, RocketLeagueModesEnum.v3)} {etxb.Username} ({etxb.v3}) {GetChangeSign(etxb.v3.Value - etxb.v3Daily.Value)}{etxb.v3.Value - etxb.v3Daily.Value}{GetETXStatus((ETXStatusEnum)etxb.ETXStauts)}" + Environment.NewLine;
                etxbCount++;
                etxbNewSumRanks += etxb.v3.Value;
                etxbOldSumRanks += etxb.v3Daily.Value;
            }

            foreach (var etxAlternate in etxAlternates)
            {
                etxFieldDesc += $"{GetETXStatus((ETXStatusEnum)etxAlternate.ETXStauts)} - {RocketLeagueUtils.GetRankEmoji(etxAlternate.v3.Value, RocketLeagueModesEnum.v3)} {etxAlternate.Username} ({etxAlternate.v3}) {GetChangeSign(etxAlternate.v3.Value - etxAlternate.v3Daily.Value)}{etxAlternate.v3.Value - etxAlternate.v3Daily.Value}" + Environment.NewLine;
            }

            foreach (var etxaAlternate in etxaAlternates)
            {
                etxaFieldDesc += $"{GetETXStatus((ETXStatusEnum)etxaAlternate.ETXStauts)} - {RocketLeagueUtils.GetRankEmoji(etxaAlternate.v3.Value, RocketLeagueModesEnum.v3)} {etxaAlternate.Username} ({etxaAlternate.v3}) {GetChangeSign(etxaAlternate.v3.Value - etxaAlternate.v3Daily.Value)}{etxaAlternate.v3.Value - etxaAlternate.v3Daily.Value}" + Environment.NewLine;
            }

            foreach (var etxbAlternate in etxbAlternates)
            {
                etxbFieldDesc += $"{GetETXStatus((ETXStatusEnum)etxbAlternate.ETXStauts)} - {RocketLeagueUtils.GetRankEmoji(etxbAlternate.v3.Value, RocketLeagueModesEnum.v3)} {etxbAlternate.Username} ({etxbAlternate.v3}) {GetChangeSign(etxbAlternate.v3.Value - etxbAlternate.v3Daily.Value)}{etxbAlternate.v3.Value - etxbAlternate.v3Daily.Value}" + Environment.NewLine;
            }

            var etxMmrChange = (etxNewSumRanks / etxCount) - (etxOldSumRanks / etxCount);
            var etxaMmrChange = (etxaNewSumRanks / etxaCount) - (etxaOldSumRanks / etxaCount);
            var etxbMmrChange = (etxbNewSumRanks / etxbCount) - (etxbOldSumRanks / etxbCount);

            EmbedFieldBuilder etxField = new EmbedFieldBuilder()
                .WithName($"{EmoteUtils.GetEmoteStringByName("EternalExodus")} **ETERNAL EXODUS | {RocketLeagueUtils.GetRankEmoji(etxNewSumRanks / etxCount, RocketLeagueModesEnum.v3)} {etxNewSumRanks / etxCount} MMR {GetChangeSign(etxMmrChange)}{etxMmrChange}**")
                .WithValue(etxFieldDesc);

            EmbedFieldBuilder etxaField = new EmbedFieldBuilder()
                .WithName($"{EmoteUtils.GetEmoteStringByName("ETXAcademy")} **ETX ACADEMY A | {RocketLeagueUtils.GetRankEmoji(etxaNewSumRanks / etxaCount, RocketLeagueModesEnum.v3)} {etxaNewSumRanks / etxaCount} MMR {GetChangeSign(etxaMmrChange)}{etxaMmrChange}**")
                .WithValue(etxaFieldDesc);

            EmbedFieldBuilder etxbField = new EmbedFieldBuilder()
                .WithName($"{EmoteUtils.GetEmoteStringByName("ETXAcademy")} **ETX ACADEMY B | {RocketLeagueUtils.GetRankEmoji(etxbNewSumRanks / etxbCount, RocketLeagueModesEnum.v3)} {etxbNewSumRanks / etxbCount} MMR {GetChangeSign(etxbMmrChange)}{etxbMmrChange}**")
                .WithValue(etxbFieldDesc);

            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(30, 144, 255))
                .WithTitle("**CLUBES ETX**")
                .WithFields(new EmbedFieldBuilder[] { etxField, etxaField, etxbField })
                .WithFooter($"Última actualización: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} (Actualización cada día a las 01:00)")
                .Build();

            return embed;
        }

        public Embed RankingEmbed(string title)
        {
            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(254, 254, 254))
                .WithTitle(title)
                .Build();

            return embed;
        }
        public Embed RocketInscriptionEmbed()
        {
            Embed embed = new EmbedBuilder()
            {
                Description = Environment.NewLine + "■ Reacciona al emoji de este mensaje para recibir un enlace al formulario que tendrás que rellenar." + Environment.NewLine + Environment.NewLine +
                              "■ Tus rangos estarán disponibles 15 minutos después de rellenar el formulario." + Environment.NewLine + Environment.NewLine +
                              "■ Puedes anularlo cuando quieras, reaccionando al emoji nuevamente." + Environment.NewLine
            }
                .WithColor(new Discord.Color(46, 204, 113))
                .WithTitle("**📝 | INSCRIPCIÓN RANKING RL**")
                .WithFooter("Inscripción Rocket League | Embed type: 5")
                .Build();

            return embed;
        }
    }
}
