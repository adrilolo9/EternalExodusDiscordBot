﻿using Discord;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.Events;
using EternalExodusDiscordBot.Commands.Classes.Rankings;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Database.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public class EmbedUtils
    {
        public static Dictionary<ulong, CancellationTokenSource> cancellationTokens = new Dictionary<ulong, CancellationTokenSource>();

        #region embed templates

        public Embed ErrorEmbed(string errorMsg)
        {
            Embed embed = new EmbedBuilder()
            {
                Description = errorMsg.Length > 2048 ? errorMsg.Substring(0, 2047) : errorMsg
            }
                .WithColor(new Discord.Color(150, 150, 150))
                .WithAuthor("An error ocurred")
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://lolo.s-ul.eu/fzFiJASw")
                .Build();

            return embed;
        }

        public Embed SuccesEmbed(string message)
        {
            Embed embed = new EmbedBuilder()
            {
                Description = message.Length > 2048 ? message.Substring(0, 2047) : message
            }
                .WithColor(new Discord.Color(254, 254, 254))
                .WithAuthor("Succes")
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://lolo.s-ul.eu/vY378asB")
                .Build();

            return embed;
        }

        public Embed BasicMessageEmbed()
        {
            Embed embed = new EmbedBuilder()
            {
                Description = ""
            }
                .WithColor(new Discord.Color(254, 254, 254))
                .WithTitle("")
                .Build();

            return embed;
        }

        public Embed PermissionErrorEmbed(string errorMsg)
        {
            Embed embed = new EmbedBuilder()
            {
                Description = errorMsg.Length > 2048 ? errorMsg.Substring(0, 2047) : errorMsg
            }
                .WithColor(new Discord.Color(150, 150, 150))
                .WithAuthor("Permission error")
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://lolo.s-ul.eu/397zr2gf")
                .Build();

            return embed;
        }

        public Embed FormEmbed(string title, string formUrl)
        {
            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(254, 254, 254))
                .WithAuthor(title)
                .WithDescription("Rellena el siguiente formulario, cuando lo hayas hecho espera unos instantes y comprueba este mensaje de nuevo para saber si se ha completado correctamente el formulario." + Environment.NewLine +
                                 "Tienes 10 minutos para rellenar el formulario." + Environment.NewLine +
                                 $"[**LINK DEL FORMULARIO**]({formUrl})")
                .WithThumbnailUrl("https://lolo.s-ul.eu/tXh458l2.gif")
                .Build();

            return embed;
        }

        public Embed UserJoinedFormEmbed(string title, string formUrl)
        {
            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(254, 254, 254))
                .WithAuthor(title)
                .WithDescription("Rellena el siguiente formulario para poder acceder al servidor. Cuando se haya revisado tu solicitud recibirás un mensaje que te informará de si se te ha concedido acceso al servidor. " + Environment.NewLine +
                                 $"[**LINK DEL FORMULARIO**]({formUrl})")
                .WithThumbnailUrl("https://lolo.s-ul.eu/pb6n0BPE.png")
                .Build();

            return embed;
        }

        public Embed SolicitudEntradaEmbed(EtxUserJoined user)
        {
            EmbedFieldBuilder idField = new EmbedFieldBuilder()
                .WithName("Discord ID")
                .WithValue($"{user.DiscordId}");
            EmbedFieldBuilder estadoField = new EmbedFieldBuilder()
                .WithName("Estado de la solicitud")
                .WithValue($"{EmoteUtils.GetEmoteStringByName("Pending")} | Pendiente");
            EmbedFieldBuilder invitadoPorField = new EmbedFieldBuilder()
                .WithName("¿Quién le ha invitado al servidor?")
                .WithValue(user.InvitadoPor);
            EmbedFieldBuilder referenciasField = new EmbedFieldBuilder()
                .WithName("Referencias en el servidor")
                .WithValue(user.ReferenciasServidor);
            EmbedFieldBuilder bautizadoPublicadorField = new EmbedFieldBuilder()
                .WithName("¿Estás bautizado o eres publicador no bautizado?")
                .WithValue(user.BautizadoPublicador);
            EmbedFieldBuilder congregacionField = new EmbedFieldBuilder()
                .WithName("Congregación")
                .WithValue(user.Congregacion);
            EmbedFieldBuilder propositoDiosField = new EmbedFieldBuilder()
                .WithName("¿Cuál es el propósito de Dios para la humanidad?")
                .WithValue(user.PropositoDios);
            EmbedFieldBuilder reinoDiosField = new EmbedFieldBuilder()
                .WithName("¿Qué es el reino de Dios?")
                .WithValue(user.ReinoDios);
            EmbedFieldBuilder jesus144kField = new EmbedFieldBuilder()
                .WithName("¿Cuántas personas van al cielo a gobernar con Jesús?")
                .WithValue(user.Jesus144k);
            EmbedFieldBuilder cualidadJehovaField = new EmbedFieldBuilder()
                .WithName("¿Cuál es la mayor cualidad de Jehová?")
                .WithValue(user.JehovaCualidad);
            EmbedFieldBuilder obraUrgenteField = new EmbedFieldBuilder()
                .WithName("¿Qué obra urgente nos manda hacer la biblia?")
                .WithValue(user.ObraUrgente);
            EmbedFieldBuilder tematicaAtalayaField = new EmbedFieldBuilder()
                .WithName("¿Cuál ha sido la temática del último articulo de la Atalaya?")
                .WithValue(user.TematicaAtalaya);

            Embed embed = new EmbedBuilder()
            {
                Description = $"{EmoteUtils.GetEmoteStringByName("Info")} | Se necesitan 2 votos para aceptar / denegar la solicitud."
            }
                .WithColor(new Discord.Color(255, 214, 51))
                .WithTitle($"Solicitud de acceso del usuario: {user.Mention}")
                .WithFooter($"Fecha solicitud: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} | Embed type: 4")
                .WithFields(new EmbedFieldBuilder[] { idField, estadoField, invitadoPorField, referenciasField, bautizadoPublicadorField, congregacionField, propositoDiosField, reinoDiosField, jesus144kField, cualidadJehovaField, obraUrgenteField, tematicaAtalayaField })
                .Build();

            return embed;
        }

        public Embed SolicitudRechazadaEmbed()
        {
            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(255, 0, 0))
                .WithAuthor("Estado solicitud de entrada a Eternal Exodus")
                .WithDescription("Lo sentimos, tu solicitud de entrada a Eternal Exodus ha sido rechazada.")
                .WithThumbnailUrl("https://lolo.s-ul.eu/SUEwD4lm.png")
                .Build();

            return embed;
        }

        public Embed SolicitudAprobadaEmbed()
        {
            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(0, 255, 0))
                .WithAuthor("Estado solicitud de entrada a Eternal Exodus")
                .WithDescription("Tu solicitud de entrada ha sido aprobada, bienvenido a Eternal Exodus!")
                .WithThumbnailUrl("https://lolo.s-ul.eu/ohLzXgqT.png")
                .Build();

            return embed;
        }

        public Embed BienvenidaEmbed()
        {
            Embed embed = new EmbedBuilder()
                .WithColor(new Discord.Color(5, 230, 188))
                .WithTitle($"**📝 ACCEDE A ETERNAL EXODUS**")
                .WithDescription("**➥ PASO 1:**  Asegúrese de haber **leído** toda la información y prohibiciones del servidor antes de continuar." + Environment.NewLine + Environment.NewLine +
                                 "**➥ PASO 2:**  Por seguridad, debes **rellenar un formulario** para que podamos conocerte mejor. [Pulsa aquí.](https://forms.gle/xypcurHoTi4LjYv4A)" + Environment.NewLine + Environment.NewLine +
                                 "**➥ PASO 3:**  Una vez rellenado, tendrás que **esperar** que el soporte humano lo revise para aceptar o denegar la solicitud.")
                .Build();

            return embed;
        }

        #endregion

        #region utils

        public int GetEmbedType(IMessage message)
        {
            try
            {
                var embed = message.Embeds.FirstOrDefault();
                var type = embed.Footer.Value.Text.Split('|')[1].Split(':')[1].Trim();
                int parsedInt;
                int.TryParse(type, out parsedInt);
                return parsedInt;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public async Task ManageEmbedPagination(int game, decimal playersCount, decimal playersPerEmbed, ISocketMessageChannel channel)
        {
            using (var db = new EternalExodusEntities())
            {
                var embedType = EmbedEnum.Ranking.GetHashCode();
                var embeds = db.RankingEmbeds.Where(x => x.GameId == game && x.EmbedType == embedType).ToList();
                if (playersCount > playersPerEmbed)
                {
                    var theoreticalEmbedsPerMode = Math.Ceiling(playersCount / playersPerEmbed);
                    var theoreticalEmbedsTotal = theoreticalEmbedsPerMode * Enum.GetValues(typeof(RocketLeagueModesEnum)).Length;
                    if (theoreticalEmbedsTotal != embeds.Count)
                    {
                        var modes = (Enum.GetValues(typeof(RocketLeagueModesEnum)) as int[]).OrderByDescending(x => x);
                        foreach (var mode in modes)
                        {
                            var modeEmbeds = embeds.Where(x => x.GameMode == mode);

                            if (modeEmbeds.Count() > theoreticalEmbedsPerMode)
                            {
                                var embedsToRemove = embeds.Where(x => x.PageNum > theoreticalEmbedsPerMode);
                                foreach (var embed in embedsToRemove)
                                {
                                    await channel.DeleteMessageAsync((ulong)embed.EmbedId);
                                }
                                db.RankingEmbeds.RemoveRange(embedsToRemove);
                                db.SaveChanges();
                            }
                            else if (modeEmbeds.Count() < theoreticalEmbedsPerMode)
                            {
                                for (int i = 0; i < theoreticalEmbedsPerMode - modeEmbeds.Count(); i++)
                                {
                                    var message = await channel.SendMessageAsync(embed: new RocketLeagueUtils().RankingEmbed($"RANKING - {RocketLeagueUtils.GetModeName((RocketLeagueModesEnum)mode)}"));
                                    db.RankingEmbeds.Add(new RankingEmbeds { EmbedId = Convert.ToInt64(message.Id), ChannelId = Convert.ToInt64(channel.Id), EmbedType = embedType, GameId = GamesEnum.RocketLeague.GetHashCode(), GameMode = mode, PageNum = modeEmbeds.Count() + 1 + i });
                                }
                                db.SaveChanges();
                            }
                        }
                        var embedsDB = db.RankingEmbeds.Where(x => x.GameId == game && x.EmbedType == embedType).OrderByDescending(x => x.GameMode).ThenBy(x => x.PageNum).ToList();
                        foreach (var embDB in embedsDB)
                        {
                            await channel.DeleteMessageAsync((ulong)embDB.EmbedId);
                        }
                        foreach (var embDB in embedsDB)
                        {
                            var message = await channel.SendMessageAsync(embed: new RocketLeagueUtils().RankingEmbed($"{embDB.PageNum} | RANKING - {RocketLeagueUtils.GetModeName((RocketLeagueModesEnum)embDB.GameMode)}"));
                            db.RankingEmbeds.Remove(embDB);
                            var newEmb = new RankingEmbeds { 
                                EmbedId = Convert.ToInt64(message.Id),
                                ChannelId = embDB.ChannelId,
                                EmbedType = embDB.EmbedType,
                                GameId = embDB.GameId,
                                GameMode = embDB.GameMode,
                                PageNum = embDB.PageNum
                            };
                            db.RankingEmbeds.Add(newEmb);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

        #endregion

    }
}
