﻿using EternalExodusDiscordBot.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public class WebUtils
    {
        private HttpClient client;

        public WebUtils()
        {
            client = new HttpClient();
        }

        public string GetPageHtml(string url)
        {
            WebResponse response;
            Stream dataStream;
            StreamReader reader;
            string responseFromServer;

            WebRequest request = (HttpWebRequest)WebRequest.Create(url);

            response = request.GetResponse();
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return responseFromServer;
        }

        public async Task<string> UploadImageToSul(MemoryStream imageData)
        {
            var content = new MultipartFormDataContent();

            var imageContent = new ByteArrayContent(imageData.ToArray());
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");

            content.Add(imageContent, "file", "rlshop.png");

            var response = await client.PostAsync($"https://s-ul.eu/api/v1/upload?wizard=true&key={BotSettings.Default.SulApiKey}", content);

            var responseString = await response.Content.ReadAsStringAsync();

            var responseDeserialized = JsonConvert.DeserializeObject<SulResponse>(responseString);

            return responseDeserialized.url;
        }

        public async Task<string> UploadImageToSul(string imagepath)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(imagepath, FileMode.Open, FileAccess.Read))
                file.CopyTo(ms);

            return await UploadImageToSul(ms);
        }

        private class SulResponse
        {
            public string domain { get; set; }
            public string filename { get; set; }
            public string protocol { get; set; }
            public string url { get; set; }
            public string extension { get; set; }
        }
    }
}
