﻿using EternalExodusDiscordBot.Properties;
using NLog;
using SteamWebAPI2.Interfaces;
using SteamWebAPI2.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public class SteamUtils
    {
        private Logger log = LogManager.GetCurrentClassLogger();
        public async Task<long?> GetSteamId64(string steamId)
        {
            try
            {
                var webInterfaceFactory = new SteamWebInterfaceFactory(BotSettings.Default.SteamWebApiKey);
                var steamInterface = webInterfaceFactory.CreateSteamWebInterface<SteamUser>(new HttpClient());

                var steam64 = await steamInterface.ResolveVanityUrlAsync(steamId);

                return Convert.ToInt64(steam64.Data);
            }
            catch (Exception ex)
            {
                log.Error($"Error obteniendo la steamId64, message: {ex.Message}");
                return null;
            }
        }
    }
}
