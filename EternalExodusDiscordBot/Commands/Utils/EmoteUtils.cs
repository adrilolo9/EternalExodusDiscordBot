﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands.Utils
{
    public static class EmoteUtils
    {
        private static DiscordSocketClient client;

        public static void SetClient(DiscordSocketClient _client)
        {
            client = _client;
        }

        public static GuildEmote GetEmoteByName(string name)
        {
            var emote = client.Guilds
            .SelectMany(x => x.Emotes)
            .FirstOrDefault(x => x.Name.IndexOf(
                name, StringComparison.OrdinalIgnoreCase) != -1);

            return emote;
        }

        public static string GetEmoteStringByName(string name)
        {
            var emote = client.Guilds
            .SelectMany(x => x.Emotes)
            .FirstOrDefault(x => x.Name.IndexOf(
                name, StringComparison.OrdinalIgnoreCase) != -1);

            return $"<:{name}:{emote.Id}>";
        }
    }
}
