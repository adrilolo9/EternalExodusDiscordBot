﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands.Classes.Rankings;
using EternalExodusDiscordBot.Commands.Classes.RocketLeague;
using EternalExodusDiscordBot.Commands.CommandDefinition.Backups;
using EternalExodusDiscordBot.Commands.CommandDefinition.Embeds;
using EternalExodusDiscordBot.Commands.CommandDefinition.Forms;
using EternalExodusDiscordBot.Commands.CommandDefinition.JW;
using EternalExodusDiscordBot.Commands.CommandDefinition.Rankings;
using EternalExodusDiscordBot.Commands.CommandDefinition.RocketLeague;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot.Commands
{
    public class DelayedCommandsDeclaration
    {
        private DiscordSocketClient _client;
        private EmbedUtils embedUtils = new EmbedUtils();
        private RocketLeagueUtils rlUtils = new RocketLeagueUtils();
        private Logger log = LogManager.GetCurrentClassLogger();

        public async Task RunCommandsWithDelay(DiscordSocketClient client)
        {
            _client = client;
            if (BotMain.ActiveBotToken == BotSettings.Default.BotToken)
            {
                RunDailyText().GetAwaiter();
                RunEmbedEvents().GetAwaiter();
                RunUpdateRocketLeagueRankings().GetAwaiter();
                RunRocketLeagueRankings().GetAwaiter();
                RunChannelMessagesBackup().GetAwaiter();
                RunRocketLeagueShop().GetAwaiter();
                RunUserJoinedCheck().GetAwaiter();

                //Deprecated
                //RunRocketLeagueEsportsShop().GetAwaiter();
                //RunUpdateLeagueOfLegendsRankings().GetAwaiter();
                //RunLeagueOfLegendsRankings().GetAwaiter();
                //RunMainRanking().GetAwaiter();
                //RunEtxAcademyRankings().GetAwaiter();
            }
            await Task.CompletedTask;
        }

        #region UserEvents

        private async Task RunUserJoinedCheck()
        {
            while (true)
            {
                try
                {
                    UserJoinedForm form = new UserJoinedForm(_client);
                    var newUsersList = await form.CheckForNewUsers();

                    if (newUsersList.Count > 0)
                    {
                        EmbedUtils embUtils = new EmbedUtils();
                        var channel = _client.GetChannel(747534258922324069) as ISocketMessageChannel;

                        foreach (var user in newUsersList)
                        {
                            var message = await channel.SendMessageAsync(embed: embUtils.SolicitudEntradaEmbed(user));
                            await message.AddReactionsAsync(new Emote[] { EmoteUtils.GetEmoteByName("Accepted"), EmoteUtils.GetEmoteByName("Denied") });
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error comprobando usuarios nuevos, exception: {e.Message}");
                }

                await Task.Delay(TimeSpan.FromMinutes(10));
            }
        }

        #endregion

        #region JW

        private async Task RunDailyText()
        {
            while (true)
            {
                TimeSpan ts = DateTime.Now > DateTime.Today.AddHours(8) ? DateTime.Today.AddDays(1).AddHours(8) - DateTime.Now : DateTime.Today.AddHours(8) - DateTime.Now;
                log.Info($"Daily text timespan set to: {ts.TotalHours} hours");

                await Task.Delay(Convert.ToInt32(Math.Round(ts.TotalMilliseconds)));
                var channel = _client.GetChannel(613742550716776473) as ISocketMessageChannel;

                try
                {
                    var message = await channel.SendMessageAsync("", embed: new DailyText().GetDailyText());
                    await message.AddReactionAsync(EmoteUtils.GetEmoteByName("buenas"));
                    await message.AddReactionAsync(EmoteUtils.GetEmoteByName("PepeOK"));
                }
                catch (Exception ex)
                {
                    log.Error($"Exception: {ex.Message}, Inner: {ex.InnerException}");
                    await channel.SendMessageAsync("", embed: embedUtils.ErrorEmbed(ex.Message));
                }
            }
        }

        #endregion

        #region Backups

        private async Task RunChannelMessagesBackup()
        {
            while (true)
            {
                TimeSpan ts = DateTime.Now > DateTime.Today.AddHours(4) ? DateTime.Today.AddDays(1).AddHours(4) - DateTime.Now : DateTime.Today.AddHours(4) - DateTime.Now;
                await Task.Delay(ts);

                try
                {
                    BackupService bs = new BackupService();

                    using (var db = new EternalExodusEntities())
                    {
                        var channels = db.BackupChannel.ToList();
                        foreach (var channel in channels)
                        {
                            await bs.BackupChannelMessages(_client, (ulong)channel.ChannelId);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error en la delayed task para realizar backups, exception: {e.Message}");
                }
            }
        }

        #endregion

        #region Embeds

        private async Task RunEmbedEvents()
        {
            try
            {
                using (var db = new EternalExodusEntities())
                {
                    var countdownEmbed = db.CountdownEmbed.Where(x => x.Ended == false).ToList();
                    foreach (var emb in countdownEmbed)
                    {
                        new CustomEmbedMessage().CountdownEmbed(emb, _client).GetAwaiter();
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Error iniciando los eventos activos de la DB, exception: {e.Message}");
            }

            await Task.CompletedTask;
        }

        private async Task RunMainRanking()
        {
            while (true)
            {
                try
                {
                    MainRanking mr = new MainRanking();

                    using (var db = new EternalExodusEntities())
                    {
                        var gameId = GamesEnum.MainRanking.GetHashCode();
                        var mainRanking = db.RankingEmbeds.FirstOrDefault(x => x.GameId == gameId);
                        if (mainRanking != null)
                        {
                            var channel = _client.GetChannel((ulong)mainRanking.ChannelId) as ISocketMessageChannel;
                            var message = channel.GetMessageAsync((ulong)mainRanking.EmbedId).Result as IUserMessage;
                            await message.ModifyAsync(x => x.Embed = mr.UpdateMainRanking(_client));

                            var rlReactionCheck = message.Reactions.FirstOrDefault(x => x.Key.Name == "RocketLeague" && x.Value.IsMe == true).Key;
                            var lolReactionCheck = message.Reactions.FirstOrDefault(x => x.Key.Name == "LeagueOfLegends" && x.Value.IsMe == true).Key;
                            if (rlReactionCheck == null)
                            {
                                await message.AddReactionAsync(EmoteUtils.GetEmoteByName("RocketLeague"));
                            }
                            if (lolReactionCheck == null)
                            {
                                await message.AddReactionAsync(EmoteUtils.GetEmoteByName("LeagueOfLegends"));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error actualizando el embed de main ranking, exception: {e.Message}");
                }

                await Task.Delay(TimeSpan.FromMinutes(BotSettings.Default.RankingsUpdateTime));
            }
        }

        #endregion

        #region RocketLeague

        private async Task RunRocketLeagueRankings()
        {
            while (true)
            {
                try
                {
                    RocketLeagueRankings rlr = new RocketLeagueRankings();

                    using (var db = new EternalExodusEntities())
                    {
                        var players = db.RankingsRocketLeague.ToList();
                        var gameId = GamesEnum.RocketLeague.GetHashCode();
                        var embedType = EmbedEnum.Ranking.GetHashCode();
                        var playersPerEmbed = 30;

                        var modes = db.RankingEmbeds.Where(x => x.GameId == gameId && x.EmbedType == embedType).ToList();

                        if (modes != null && modes.Count > 0)
                        {
                            var channel = _client.GetChannel((ulong)modes.FirstOrDefault().ChannelId) as ISocketMessageChannel;
                            await embedUtils.ManageEmbedPagination(GamesEnum.RocketLeague.GetHashCode(), players.Count, playersPerEmbed, channel);

                            modes = db.RankingEmbeds.Where(x => x.GameId == gameId && x.EmbedType == embedType).ToList();

                            foreach (var mode in modes)
                            {
                                var message = channel.GetMessageAsync((ulong)mode.EmbedId).Result as IUserMessage;
                                var embed = message.Embeds.FirstOrDefault();

                                await message.ModifyAsync(x => x.Embed = rlr.UpdateEmbedRanking(embed, players.OrderByDescending(y => y.GetType().GetProperty(Enum.GetName(typeof(RocketLeagueModesEnum), mode.GameMode.Value)).GetValue(y)).Skip((mode.PageNum.Value - 1) * playersPerEmbed).Take(playersPerEmbed).ToList(), mode.GameMode.Value, (mode.PageNum.Value - 1) * playersPerEmbed));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error actualizando el embed de los rankings de rocket league, exception: {e.Message}");
                }

                await Task.Delay(TimeSpan.FromMinutes(BotSettings.Default.RankingsUpdateTime));
            }
        }

        private async Task RunUpdateRocketLeagueRankings()
        {
            while (true)
            {
                try
                {
                    RocketStats rs = new RocketStats();

                    using (var db = new EternalExodusEntities())
                    {
                        var players = db.RankingsRocketLeague.ToList();
                        foreach (var player in players)
                        {
                            rs.GetPlayerStats(player);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error en la delayed task para actualizar los rangos de Rocket League, exception: {e.Message}");
                }

                await Task.Delay(TimeSpan.FromMinutes(BotSettings.Default.RankingsUpdateTime));
            }
        }

        private async Task RunEtxAcademyRankings()
        {
            while (true)
            {
                TimeSpan ts = DateTime.Now > DateTime.Today.AddHours(1) ? DateTime.Today.AddDays(1).AddHours(1) - DateTime.Now : DateTime.Today.AddHours(1) - DateTime.Now;
                await Task.Delay(ts);

                try
                {
                    using (var db = new EternalExodusEntities())
                    {
                        RocketStats rs = new RocketStats();
                        var gameId = GamesEnum.EtxAcademy.GetHashCode();
                        var embedType = EmbedEnum.Ranking.GetHashCode();
                        var embed = db.RankingEmbeds.FirstOrDefault(x => x.GameId == gameId && x.EmbedType == embedType);
                        if (embed != null)
                        {
                            var etxPlayers = db.RankingsRocketLeague.Where(x => x.ETXAcademy != 0).ToList();
                            var channel = _client.GetChannel((ulong)embed.ChannelId) as ISocketMessageChannel;
                            var message = channel.GetMessageAsync((ulong)embed.EmbedId).Result as IUserMessage;
                            await message.ModifyAsync(x => x.Embed = rlUtils.EtxAcademyRankingsEmbed(etxPlayers));
                        }

                        rs.UpdateDailyRanks();
                        rs.UpdateETXAcademyPositions();
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error actualizando el embed de la ETX Academy, exception: {e.Message}");
                }
            }
        }

        #endregion

        #region LeagueOfLegends

        private async Task RunUpdateLeagueOfLegendsRankings()
        {
            while (true)
            {
                try
                {
                    LeagueOfLegendsRankings lolRanks = new LeagueOfLegendsRankings();

                    using (var db = new EternalExodusEntities())
                    {
                        var players = db.RankingsLeagueOfLegends.ToList();
                        foreach (var player in players)
                        {
                            await lolRanks.UpdateSummonerStats(player);
                            await Task.Delay(500);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error actualizando los stats de los summoners de LOL, exception: {e.Message}");
                }

                await Task.Delay(TimeSpan.FromMinutes(BotSettings.Default.RankingsUpdateTime));
            }
        }

        private async Task RunLeagueOfLegendsRankings()
        {
            while (true)
            {
                try
                {
                    LeagueOfLegendsRankings lolRanks = new LeagueOfLegendsRankings();

                    using (var db = new EternalExodusEntities())
                    {
                        var players = db.RankingsLeagueOfLegends.ToList();
                        var gameId = GamesEnum.LeagueOfLegends.GetHashCode();
                        var embedType = EmbedEnum.Ranking.GetHashCode();

                        var modes = db.RankingEmbeds.Where(x => x.GameId == gameId && x.EmbedType == embedType).ToList();
                        foreach (var mode in modes)
                        {
                            var channel = _client.GetChannel((ulong)mode.ChannelId) as ISocketMessageChannel;
                            var message = channel.GetMessageAsync((ulong)mode.EmbedId).Result as IUserMessage;
                            var embed = message.Embeds.FirstOrDefault();

                            await message.ModifyAsync(x => x.Embed = lolRanks.UpdateEmbedRanking(_client, embed, lolRanks.OrderPlayers(players.ToList(), mode.GameMode.Value), mode.GameMode.Value));
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"Error actualizando embed de rankings LOL, exception: {e.Message}");
                }

                await Task.Delay(TimeSpan.FromMinutes(BotSettings.Default.RankingsUpdateTime));
            }
        }

        #endregion

        #region DailyShops

        private async Task RunRocketLeagueShop()
        {
            while (true)
            {
                TimeSpan ts = DateTime.Now > DateTime.Today.AddHours(22).AddMinutes(30) ? DateTime.Today.AddDays(1).AddHours(22).AddMinutes(30) - DateTime.Now : DateTime.Today.AddHours(22).AddMinutes(30) - DateTime.Now;
                log.Info($"Rocket League shop timespan set to: {ts.TotalHours} hours");

                await Task.Delay(Convert.ToInt32(Math.Round(ts.TotalMilliseconds)));
                var channel = _client.GetChannel(582181204594262016) as ISocketMessageChannel;

                int retries = 0;
                bool success = false;

                while (retries < 6 && success == false)
                {
                    retries++;
                    try
                    {
                        var message = await channel.SendFileAsync(new RocketShop().GetItemShop());
                        await message.AddReactionAsync(new Emoji("👍"));
                        await message.AddReactionAsync(new Emoji("👎"));
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Exception: {ex.Message}, Inner: {ex.InnerException}");
                        if (retries == 5)
                        {
                            await channel.SendMessageAsync("", embed: embedUtils.ErrorEmbed(ex.Message));
                        }
                        else
                        {
                            await Task.Delay(TimeSpan.FromMinutes(10));
                        }
                    }
                }
            }
        }

        private async Task RunRocketLeagueEsportsShop()
        {
            while (true)
            {
                TimeSpan ts = DateTime.Now > DateTime.Today.AddHours(22).AddMinutes(30) ? DateTime.Today.AddDays(1).AddHours(22).AddMinutes(30) - DateTime.Now : DateTime.Today.AddHours(22).AddMinutes(30) - DateTime.Now;
                log.Info($"Rocket League esports shop timespan set to: {ts.TotalHours} hours");

                await Task.Delay(Convert.ToInt32(Math.Round(ts.TotalMilliseconds)));
                var channel = _client.GetChannel(582181204594262016) as ISocketMessageChannel;

                int retries = 0;
                bool success = false;

                while (retries < 6 && success == false)
                {
                    retries++;
                    try
                    {
                        var message = await channel.SendFileAsync(new RocketEsportsShop().GetRocketEShop());
                        await message.AddReactionAsync(new Emoji("👍"));
                        await message.AddReactionAsync(new Emoji("👎"));
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Exception: {ex.Message}, Inner: {ex.InnerException}");
                        if (retries == 5)
                        {
                            await channel.SendMessageAsync("", embed: embedUtils.ErrorEmbed(ex.Message));
                        }
                        else
                        {
                            await Task.Delay(TimeSpan.FromMinutes(10));
                        }
                    }
                }
            }
        }

        #endregion
    }
}
