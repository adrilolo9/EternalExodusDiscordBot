//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EternalExodusDiscordBot.Database.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class RankingsLeagueOfLegends
    {
        public long DiscordId { get; set; }
        public string SummonerId { get; set; }
        public string SummonerAccountId { get; set; }
        public string SummonerPuuid { get; set; }
        public int Region { get; set; }
        public string TierSolo { get; set; }
        public string TierFlex { get; set; }
        public string RankSolo { get; set; }
        public string RankFlex { get; set; }
        public Nullable<int> LpSolo { get; set; }
        public Nullable<int> LpFlex { get; set; }
    }
}
