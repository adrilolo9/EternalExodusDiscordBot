﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EternalExodusDiscordBot.Commands;
using EternalExodusDiscordBot.Commands.CommandDefinition.Forms;
using EternalExodusDiscordBot.Commands.Utils;
using EternalExodusDiscordBot.Database.Model;
using EternalExodusDiscordBot.Properties;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EternalExodusDiscordBot
{
    public class BotMain
    {
        #region Classes and properties declaration

        private CommandService _cmdService;
        private DiscordSocketClient _client;
        private CancellationTokenSource cancellationToken;
        private Logger log = LogManager.GetCurrentClassLogger();
        private HandlerAddedReactions handlerAddedReactions;
        private HandlerRemovedReactions handlerRemovedReactions;

        public static string ActiveBotToken = BotSettings.Default.BotToken;

        #endregion

        public async Task MainAsync()
        {
            //Creating instances
            _cmdService = new CommandService();
            cancellationToken = new CancellationTokenSource();

            //Client config
            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Info,
                MessageCacheSize = 20,
                AlwaysDownloadUsers = true
            });

            //Setting up handlers
            _client.Log += Log;
            if (ActiveBotToken == BotSettings.Default.BotToken)
            {
                handlerAddedReactions = new HandlerAddedReactions(_client);
                handlerRemovedReactions = new HandlerRemovedReactions(_client);

                _client.ReactionAdded += handlerAddedReactions.Handle;
                _client.ReactionRemoved += handlerRemovedReactions.Handle;
                //_client.UserJoined += handleUserJoined;
                _client.UserLeft += handleUserLeft;
            }

            await InstallCommands();

            //Start bot
            await _client.LoginAsync(TokenType.Bot, ActiveBotToken);
            await _client.StartAsync();

            EmoteUtils.SetClient(_client);
            await new DelayedCommandsDeclaration().RunCommandsWithDelay(_client);

            // Block this task until the program is closed.
            await Task.Delay(-1, cancellationToken.Token);
        }

        private async Task handleUserJoined(SocketGuildUser user)
        {
            //UserJoinedForm form = new UserJoinedForm(_client);
            //await form.SendForm(user);
        }

        private async Task handleUserLeft(SocketGuildUser user)
        {
            using (var db = new EternalExodusEntities())
            {
                var userId = Convert.ToInt64(user.Id);
                var rlUser = db.RankingsRocketLeague.FirstOrDefault(x => x.DiscordId == userId);
                if (rlUser != null)
                {
                    db.RankingsRocketLeague.Remove(rlUser);
                }
                var lolUser = db.RankingsLeagueOfLegends.FirstOrDefault(x => x.DiscordId == userId);
                if (lolUser != null)
                {
                    db.RankingsLeagueOfLegends.Remove(lolUser);
                }

                db.SaveChanges();
            }

            await Task.CompletedTask;
        }

        public async Task InstallCommands()
        {
            // Hook the MessageReceived Event into our Command Handler
            _client.MessageReceived += HandleCommand;
            // Discover all of the commands in this assembly and load them.
            await _cmdService.AddModulesAsync(Assembly.GetEntryAssembly(), null);
        }

        public async Task HandleCommand(SocketMessage messageParam)
        {
            // Don't process the command if it was a System Message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;
            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;
            // Determine if the message is a command, based on if it starts with '!' or a mention prefix
            if (!(message.HasCharPrefix((ActiveBotToken == BotSettings.Default.BotToken ? '&' : '%'), ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))) {

                return;
            }
            // Create a Command Context
            var context = new CommandContext(_client, message);
            // Execute the command. (result does not indicate a return value,   
            // rather an object stating if the command executed succesfully)
            var result = await _cmdService.ExecuteAsync(context, argPos, null);
            if (!result.IsSuccess)
                await context.Channel.SendMessageAsync(result.ErrorReason);
        }

        public async Task Log(LogMessage arg)
        {
            log.Info(arg.Message);
            await Task.CompletedTask;
        }

        public async Task Start()
        {
            await MainAsync();
        }

        public void Stop()
        {
            cancellationToken.Cancel();
        }
    }
}
